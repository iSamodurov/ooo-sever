<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контактная информация");
?> 
<h3>Контакты сотрудников</h3>
 
<div class="contacts-wrapper"> 
  <div class="contact-item"> <b class="name">Компания &quot;Север Опт&quot;</b> 
    <div class="contact">Тел: +7 (4922) 377-277</div>
   
    <div class="contact">Тел: +7 (4922) 37-31-75</div>
   
    <div class="contact">E-mail: info@sever33.ru</div>
    <div class="contact">Адрес: Владимир, ул. Куйбышева, 66Б</div>

   </div>
 
  <div class="contact-item"> <b class="name">Михаил Епифанов (Отдел продаж)</b> 
    <div class="contact">Специалист по оборудованию</div>
   
<!-- <div class="contact">Специалист по оборудованию</div> -->
 
    <div class="contact">Тел: +7 (4922) 377-277 (доб. 112)</div>
   
    <div class="contact">E-mail: <a href="mailto:epifanov@sever33.ru" >epifanov@sever33.ru</a></div>
   
    <div class="contact">Время работы отдела: 
      <br />
     Пн.-Пт.: с 9:00 до 18:00 
      <br />
     Cб.-Вс.: Выходной </div>
   </div>
 
  <div class="contact-item"> <b class="name">Батаршин Равиль (Магазин)</b> 
<!-- <div class="contact"></div> -->
 
    <div class="contact">Специалист по комплектующим и расходным материалам</div>
   
    <div class="contact">Тел: +7 (4922) 377-277 (доб. 113)</div>
   
    <div class="contact">E-mail: <a href="mailto:batarshin@sever33.ru" >batarshin@sever33.ru</a></div>
   
    <div class="contact"> Время работы отдела: 
      <br />
     Пн.-Пт.: с 9:00 до 18:00 
      <br />
     Сб.: с 9:00 до 15:00 
      <br />
     Вс.: выходной </div>
   </div>
 
  <div class="contact-item"> <b class="name">Попова Оксана (Сервистно-монтажный отдел)</b> 
<!-- <div class="contact"></div> -->
 
    <div class="contact">Менеджер по работе с клиентами</div>
   
    <div class="contact">Тел: +7 (4922) 377-277 (доб. 116)</div>
   
    <div class="contact">E-mail: <a href="mailto:popova@sever33.ru" >popova@sever33.ru</a></div>
   
    <div class="contact"> Время работы отдела: 
      <br />
     Пн.-Пт.: с 9:00 до 18:00 
      <br />
     Сб.-Вс.: выходной </div>
   </div>
 
  <div class="contact-item"> <b class="name">Новичихин Алексей (Сервистно-монтажный отдел)</b> 
<!-- <div class="contact"></div> -->
 
    <div class="contact">Главный инженер</div>
   
    <div class="contact">Тел: +7 (4922) 377-277 (доб. 114)</div>
   
    <div class="contact">E-mail: <a href="mailto:ing@sever33.ru" >ing@sever33.ru</a></div>
   
    <div class="contact"> Время работы отдела: 
      <br />
     Пн.-Пт.: с 9:00 до 18:00 
      <br />
     Сб.-Вс.: выходной </div>
   </div>
 
  <div class="contact-item"> <b class="name">Рапохина Надежда (Бухгалтерский отдел)</b> 
<!-- <div class="contact"></div> -->
 
    <div class="contact">Главный Бухгалтер</div>
   
    <div class="contact">Тел: +7 (4922) 377-277 (доб. 111)</div>
   
    <div class="contact">E-mail: <a href="mailto:fin@sever33.ru" >fin@sever33.ru</a></div>
   
    <div class="contact"> Время работы отдела: 
      <br />
     Пн.-Пт.: с 9:00 до 17:00 
      <br />
     Сб.-Вс.: выходной </div>
   </div>
 </div>
 
<h2>Понорамный обзор и карта проезда</h2>
 
<div class="toggle_map"> <button class="blue_btn" onclick="showMap();">Карта проезда</button> <button class="blue_btn" onclick="showPano();">Понорамный обзор</button> </div>
 
<div class="maps"> 
  <div class="map" id="my_map"> <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d35550.55816879462!2d40.412444246243034!3d56.15866267694938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m5!1s0x414c7bef728254bb%3A0xfef94e32c1355cae!2z0YPQu9C40YbQsCDQk9C-0YDRjNC60L7Qs9C-LCDQktC70LDQtNC40LzQuNGALCDQktC70LDQtNC40LzQuNGA0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsINCg0L7RgdGB0LjRjw!3m2!1d56.144377999999996!2d40.3854712!4m3!3m2!1d56.1725704!2d40.4487234!5e0!3m2!1sru!2sru!4v1476731561328" width="940" height="600" frameborder="0" style="border: 0px;" allowfullscreen=""></iframe> </div>
 
  <div class="ponorama" id="ponorama" style="display: none;"> <iframe src="https://www.google.com/maps/embed?pb=!1m0!3m2!1sru!2s!4v1475416262471!6m8!1m7!1sz2_SSRPDpJ7q60MJOC2hgQ!2m2!1d56.1729385714286!2d40.44858834325035!3f185.75428084025367!4f9.707630476292394!5f0.7820865974627469" width="940" height="600" frameborder="0" style="border: 0px;" allowfullscreen=""></iframe> </div>
 </div>
 
<script>
    function showPano(){
        $('#ponorama').fadeIn(300);
        $('#my_map').fadeOut(300);
    }
    function showMap(){
        $('#my_map').fadeIn(300);
        $('#ponorama').fadeOut(300);
    }
</script>
 
<!--
<p><b><font color="#555555">Уважаемые клиенты! </font></b> 
  <br />
 Прежде чем задать свой вопрос, обратите внимание на раздел <a id="bxid_236151" href="/faq/" >Вопросы и ответы</a>. Возможно, там уже есть исчерпывающая информация по решению вашей проблемы.</p>
-->
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>