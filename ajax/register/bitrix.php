<?php

if (empty($action)) {
    return;
}

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/mainpage.php");

CModule::IncludeModule('main');
CModule::IncludeModule('iblock');

if (function_exists($action)) {
    $result = $action();
    if (!empty($result)) {
        $json = json_encode($result);
        echo $json;
    }
    return;
} else {
    echo json_encode(array('error' => 'error'));
    return;
}

function logout() {
    global $USER;
    $USER->Logout();
}