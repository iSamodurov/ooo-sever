<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/about/partners/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/partners/index.php",
	),
	array(
		"CONDITION" => "#^/about/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/news/index.php",
	),
	array(
		"CONDITION" => "#^/about/idea/#",
		"RULE" => "",
		"ID" => "bitrix:idea",
		"PATH" => "/about/idea/index.php",
	),
	array(
		"CONDITION" => "#^/components/#",
		"RULE" => "",
		"ID" => "sever:catalog",
		"PATH" => "/components/index.php",
	),
	array(
		"CONDITION" => "#^/about/jobs/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/jobs/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/news/index.php",
	),
	array(
		"CONDITION" => "#^/help/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/help/index.php",
	),
);

?>