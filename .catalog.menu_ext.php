<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
CModule::IncludeModule("iblock");

$aMenuLinks[] = Array(
	"Оборудование",
	"/catalog/",
	Array(),
	Array("DEPTH_LEVEL" => 1),
	""
);
$aMenuLinksExt = $APPLICATION->IncludeComponent(
	"bitrix:menu.sections",
	"",
	Array(
		"IS_SEF" => "Y",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "3",
		"DEPTH_LEVEL" => "2",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);
foreach($aMenuLinksExt as &$menuLink){
	$menuLink[3]["DEPTH_LEVEL"] =intval($menuLink[3]["DEPTH_LEVEL"]) + 1;
}
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);

$aMenuLinks[] = Array(
	"Комплектующие",
	"/components/",
	Array(),
	Array("DEPTH_LEVEL" => 1),
	""
);
$aMenuLinksExt = $APPLICATION->IncludeComponent(
	"bitrix:menu.sections",
	"",
	Array(
		"IS_SEF" => "Y",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "5",
		"DEPTH_LEVEL" => "2",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600"
	)
);
foreach($aMenuLinksExt as &$menuLink){
	$menuLink[3]["DEPTH_LEVEL"] =intval($menuLink[3]["DEPTH_LEVEL"]) + 1;
}
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>