<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Персональный раздел");
?>

<section class="work-area">
    <div class="container">
        <h2 align="center">Личный кабинет</h2>
        
        <p>В личном кабинете Вы можете проверить текущее состояние корзины, ход выполнения Ваших заказов, просмотреть или изменить личную информацию, а также подписаться на новости и другие информационные рассылки.</p>
                
        <aside class="left-side">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "leftPersonal", array(
                "ROOT_MENU_TYPE" => "leftPersonal",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "36000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "leftPersonal",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ),
                false
            );?>
        </aside>
    </div>
</section>

 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>