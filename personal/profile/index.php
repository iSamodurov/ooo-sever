<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Настройки пользователя");
?>
<section class="work-area">
    <div class="container">
        <aside class="left-side">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "leftPersonal", array(
                "ROOT_MENU_TYPE" => "leftPersonal",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_TIME" => "36000",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MAX_LEVEL" => "2",
                "CHILD_MENU_TYPE" => "leftPersonal",
                "USE_EXT" => "Y",
                "DELAY" => "N",
                "ALLOW_MULTI_SELECT" => "N"
            ),
                false
            );?>
        </aside>

        <div class="right-side">

            <?$APPLICATION->IncludeComponent("bitrix:main.profile", ".default", Array(
                "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
                ),
                false
            );?>
        </div>
    </div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>