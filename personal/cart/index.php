<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>

<section class="work-area">
	<div class="container">
		<aside class="left-side">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "leftPersonal", array(
				"ROOT_MENU_TYPE" => "leftPersonal",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "36000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MAX_LEVEL" => "2",
				"CHILD_MENU_TYPE" => "leftPersonal",
				"USE_EXT" => "Y",
				"DELAY" => "N",
				"ALLOW_MULTI_SELECT" => "N"
			),
				false
			);?>
		</aside>

		<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket", "", array(
			"COLUMNS_LIST" => array(
				0 => "NAME",
				1 => "DISCOUNT",
				2 => "WEIGHT",
				3 => "QUANTITY",
				4 => "DELETE",
				5 => "DELAY",
				6 => "TYPE",
				7 => "PRICE",
			),
			"OFFERS_PROPS" => array(
			),
			"PATH_TO_ORDER" => "/personal/order/make/",
			"HIDE_COUPON" => "N",
			"PRICE_VAT_SHOW_VALUE" => "N",
			"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
			"USE_PREPAYMENT" => "N",
			"QUANTITY_FLOAT" => "N",
			"SET_TITLE" => "Y"
			),
			false
		);?>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>