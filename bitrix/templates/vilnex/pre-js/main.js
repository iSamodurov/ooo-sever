

$(document).ready(function(){


    $('.card_image_slider').slick({
        asNavFor: '.card_image_slider_nav',
        prevArrow: '<i class="fa fa-chevron-left slick-prev" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-chevron-right slick-next" aria-hidden="true"></i>'
    });
    $('.card_image_slider_nav').slick({
        arrows: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        //centerMode: true,
        focusOnSelect: true,
        asNavFor: '.card_image_slider',
        infinite: true
    });


    // TABS IN DETAIL PAGE 
    var tab = $('.tab-ctrl').find('.curr a').attr('href');
    $(tab).show();

    $('.tab-ctrl-item a').click(function(){
        $('.tab-cont-item').hide();
        $('.tab-ctrl-item').removeClass('curr');
        $(this).parent().addClass('curr');

        var tab = $(this).attr('href');
        $(tab).show();
        return false;
    });


    $('.nav_wrapper ul li').hover(
        function () {
            $(this).siblings('li').find('.subnav_elements').removeClass('animated flipInX');
            $(this).find('.subnav_elements').addClass('animated flipInX');
        }
    );

    
    $(".expandet_text").append('<div class="expandet_text__action"><button class="expandet_text__btn">Читать весь текст</button></div>');
    $(".expandet_text__btn").click(function(){
        $(this).parent().parent('.expandet_text').toggleClass('expandet_text__showed');
    });


    
    var counter = 0;
    $(".bx_filter_container").map(function(elem){
        counter++;
        if(counter <= 2) {
            console.log($(this));
            $(this).addClass('active');
            $(this).find('.bx_filter_block').css({'display':'block'});
        }
    });



});


// GOOGLE MAP
var map;
var styles = [{
    "featureType":"all",
    "elementType":"all",
    "stylers":[
        {"saturation":-100},
        {"gamma":0.5}
    ]
}];

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 56.173919, lng: 40.442178},
        zoom: 15,
        disableDefaultUI:true,
        scrollwheel: false,
        styles: styles
    });

    var marker = new google.maps.Marker({
        position: {lat: 56.172803, lng: 40.448583},
        title:"Hello World!",
        map:map,
        icon: 'img/label.png'
    });
}
