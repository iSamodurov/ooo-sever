<? global $USER; ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru" xml:lang="ru">
	<head>
		<title><? $APPLICATION->ShowTitle() ?> </title>
		<? $APPLICATION->ShowHead(); ?>
        
		<base href="http://sever33.ru/bitrix/templates/vilnex/">
        
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
		<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">

    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    	<script src="https://use.fontawesome.com/43480d80b0.js"></script>
    	<script src="<?=SITE_TEMPLATE_PATH?>/pre-js/main.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

	</head>
<body>
<? $APPLICATION->ShowPanel(); ?>
<div class="blur-wrapper">


<section class="darkline">
    <div class="container">
        <ul>
            <li>Оснащаем и обслуживаем магазины</li>
            <li>Столовые</li>
            <li>Кафе</li>
            <li>Гостиницы и рестораны</li>
        </ul>
    </div>
</section>


<section class="header">
    <div class="container">
        <div class="logo">
            <a href="/">
                <img src="img/logo.png" alt="Север лого">
            </a>
        </div>
        <div class="actions">
            <div class="loginreg">
                <? if($USER->IsAuthorized()): ?>
                    <a href="/personal/">Личный кабинет</a> <br>
                <? else: ?>
                    <a href="/auth/">Вход</a> / <a href="/auth/?register=yes">Регистрация</a> <br>
                <? endif;?>
                <a href="mailto:info@sever33.ru" class="link">info@sever33.ru</a>
            </div>
            <div class="cart">
                <a href="/personal/cart/">
                <span class="cart-ico"></span>

                <?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "header_basket", Array(
                	"PATH_TO_BASKET" => "/personal/cart/",	// Страница корзины
                	"PATH_TO_ORDER" => "/personal/order/make/",	// Страница оформления заказа
                	"SHOW_DELAY" => "Y",	// Показывать отложенные товары
                	"SHOW_NOTAVAIL" => "Y",	// Показывать товары, недоступные для покупки
                	"SHOW_SUBSCRIBE" => "Y",	// Показывать товары, на которые подписан покупатель
                	),
                	false
                );?>

                </a>

            </div>
        </div>
        <div class="phone">
            <div class="phone-row">
                <span class="phone-ico animated infinite rubberBand"></span>
                <a href="tel:+74922377277" class="number">+7(4922)377-277</a>
            </div>
            <button class="btn_link callback-btn-modal">Обратный звонок</button>
        </div>
    </div>
</section>


<section class="navigation">
    <div class="container">
        <!--
        <div class="nav_wrapper">
            <ul>
                <li><a href="">О компании</a></li>
                <li>
                    <a href="">Каталог оборудования</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                    <div class="subnav_elements">
                        <div class="element">
                            <span class="ico"><img src="img/icons/fridge.png" alt=""></span>
                            <a href="">Торговое и <br>холодильное <br>оборудование</a>
                        </div>
                        <div class="element">
                            <span class="ico"><img src="img/icons/snow.png" alt=""></span>
                            <a href="">Промышленное <br>холодильное <br>оборудование</a>
                        </div>
                        <div class="element">
                            <span class="ico"><img src="img/icons/robot.png" alt=""></span>
                            <a href="">Технологическое <br> оборудование</a>
                        </div>
                        <div class="element">
                            <span class="ico"><img src="img/icons/table.png" alt=""></span>
                            <a href="">Нейтральное <br>оборудование</a>
                        </div>
                        <div class="element">
                            <span class="ico"><img src="img/icons/conditioner.png" alt=""></span>
                            <a href="">Климатическая <br> техника</a>
                        </div>
                        <div class="element">
                            <span class="ico"><img src="img/icons/sun.png" alt=""></span>
                            <a href="">Тепловое <br>оборудование</a>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="">Комплектующие</a><i class="fa fa-caret-down" aria-hidden="true"></i>
                </li>
                <li><a href="">Наши услуги</a></li>
                <li><a href="">В помощь клиенту</a></li>
                <li><a href="">Контакты</a></li>
            </ul>
            <!--
            <div class="search_form">
                <form action="">
                    <input type="text" class="search_input" name="" placeholder="Поиск">
                    <button class="go_search"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            --
        </div>
        -->

        <div class="nav_wrapper">
        <?$APPLICATION->IncludeComponent("bitrix:menu", "horizontal_multilev", array(
        	"ROOT_MENU_TYPE" => "top",
        	"MENU_CACHE_TYPE" => "N",
        	"MENU_CACHE_TIME" => "3600",
        	"MENU_CACHE_USE_GROUPS" => "Y",
        	"MENU_CACHE_GET_VARS" => array(
        	),
        	"MAX_LEVEL" => "2",
        	"CHILD_MENU_TYPE" => "sub",
        	"USE_EXT" => "Y",
        	"DELAY" => "N",
        	"ALLOW_MULTI_SELECT" => "N"
        	),
        	false
        );?>
        </div>
    </div>
</section>


<?php $page = $APPLICATION->GetCurPage(); ?>
<? if($page != '/'): ?>

<section class="breadcrumbs">
    <div class="container">
        <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumbs", Array(
	"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
	"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
	"SITE_ID" => "-",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
	),
	false
);?>
    </div>
</section>

<?php endif; ?>



<section class="work_area">
	<div class="container">