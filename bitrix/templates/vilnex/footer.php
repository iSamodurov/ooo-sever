<div class="clearfix"></div>
</div>
</section>




    <?php $page = $APPLICATION->GetCurPage(); ?>
    <? if($page == '/'): ?>

        <div class="contacts">
            <div class="contacts-header">
                <div class="container">
                    <h3>Как нас найти</h3>
                </div>
            </div>
            <div class="map" id="map"></div>

            <div class="phones-wrapper">
                <h4>Контакты</h4>
                <div class="row">e-mail: info@sever33.ru</div>
                <div class="row">
                    +7 (4922) 37-31-75 <br>
                    +7 (4922) 377-277
                </div>
                <div class="row">
                    г. Владимир, ул. Куйбышева, <br>
                    66-Б (Центральный офис)
                </div>
            </div>
        </div>

    <?php endif; ?>
    


<div class="footer">
    <div class="container">
    <!--
        <div class="user-alert">
            <p>
                Обращаем Ваше внимание на то, что приведенные на сайте sever33.ru цены и характеристики товаров носят исключительно ознакомительный характер и не являются публичной офертой,определенной пунктом 2 статьи 437 Гражданского кодекса Российской Федерации.
            </p>
        </div>
    -->
        <div class="copy">
            2009 - <?= date('Y'); ?> © Компания «Север» <br>
            Профессиональная торгово-сервисная компания в сфере холодильного оборудования и климатической техники
        </div>

        <div class="callback">
            <a href="tel:+74922377277" class="phone">+7 (4922) 377-277</a> <br>
            <a href="/#" class="phone">Заказать обратный звонок</a>
        </div>
    </div>
</div>


    


</div><!--blur-wrapper end-->



<!-- MODAL WINDOWS -->
<span style="display:none;">
    <div class="callback-form" id="callback-form">
        <h3 class="form-title">
            Оставьте свои контакты и наш специалист свяжется с вами для бесплатной консультации
        </h3>
        <form class="vertical-form ajaxForm" action="/php/submit.php">
            <input type="text" name="name" required placeholder="Имя:">
            <input type="email" name="email_2" required placeholder="E-mail:">
            <input type="tel" name="phone" placeholder="Телефон:" required>
            <button type="submit" class="blue_btn">Оставить заявку</button>
            <input type="email" name="email" value="" style="display:none;">
            <input type="hidden" name="action" value="Запрос на бесплатную консультацию из всплывающей формы">
        </form>
    </div>

    <div class="callback-form" id="ty-modal">
        <h3 class="form-title">
            Спасибо, ваша заявка успешно принята! Наш менеджер скоро свяжется с вами.
        </h3>
    </div>

</span>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBaityRL7dvvZNuTum0vaHIjTSgAUh_sAs&callback=initMap" async defer></script>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/vendor/fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    	  $("input[type=tel]").mask("+9 (999) 999-99-99",{placeholder:"+7 (___) ___-__-__"});
    });

    $('.callback-btn-modal').click(function(){
        $.fancybox({
            href: '#callback-form',
            beforeShow: function(){  $('.blur-wrapper').addClass('blured');  },
            beforeClose: function(){  $('.blur-wrapper').removeClass('blured');  }
        });
    });


    // Sand Form data
    $('.ajaxForm').submit(function(){
    	var data = $(this).serialize();
    	var link = $(this).attr('action');

    	$.ajax({
    		data: data,
    		url: link,
    		method: 'POST',
    		success: function(){
    			$.fancybox({
		            href: '#ty-modal',
		            beforeShow: function(){  $('.blur-wrapper').addClass('blured');  },
		            beforeClose: function(){  $('.blur-wrapper').removeClass('blured');  }
		        });
    		}
    	});
    	return false;
    });


    // Catalog Sort
    $("#catalog_sort_form").submit(function(){
        var page = $(this).find('input[name=page]').val();
        var method = $(this).find('#method').val();
        var sort = $(this).find('#sort').val();
        location.href = page + '?sort=' + sort + '&method=' + method;
        return false;
    });

    $('.view_tools a').click(function(){
        var link = $(this).attr('href');
        $.get(link, function(){
            location.href = link;
        });
        return false;
    });


</script>



<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'PnPjMu6Flz';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter41894879 = new Ya.Metrika({ id:41894879, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/41894879" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</body>
</html>