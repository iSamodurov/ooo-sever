<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(0 < $arResult["SECTIONS_COUNT"]):?>

<ul class="cl">
<?
$prevdepth = 0;
$i=0;

foreach ($arResult['SECTIONS'] as $arSection)
{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));

				if($arSection["DEPTH_LEVEL"]==1)
				{
				
					if (false === $arSection['PICTURE'])
						$arSection['PICTURE'] = array(
							'SRC' => '/bitrix/templates/sever/components/bitrix/catalog.section.list/components/images/line-empty.png'
						);
					
					if($arSection["DEPTH_LEVEL"]<$prevdepth) echo '</ul></div></li>';					
					elseif($arSection["DEPTH_LEVEL"]==$prevdepth) echo '</li>';
					
					?>
				
							<li<?=$i==4?' class="last"':'';?>>
								<a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" title="<? echo $arSection['NAME']; ?>" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
									<span class="img">
										<img src="<? echo $arSection['PICTURE']['SRC']; ?>" alt="<? echo $arSection['NAME']; ?>" />
									</span>
									<span class="link"><? echo $arSection['NAME']; ?></span>
								</a>
								<div class="list">
				<?	
					$i++;
				}
				elseif($arSection["DEPTH_LEVEL"]==2)
				{
					if($arSection["DEPTH_LEVEL"]>$prevdepth)
					{?>
						
						<ul>					
					<?}
					?>
						<li><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>" title="<? echo $arSection['NAME']; ?>"><? echo $arSection['NAME']; ?></a></li>
					<?
				
				}
				
				$prevdepth = $arSection["DEPTH_LEVEL"];
				
				if($i>4) break;
				
}

if($prevdepth==2) echo '</ul></div></li>';					
elseif($prevdepth==1) echo '</li>';

?>
</ul>
<?endif;?>
