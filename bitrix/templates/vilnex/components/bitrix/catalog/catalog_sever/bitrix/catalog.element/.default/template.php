<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//echo "<pre>"; 
//print_r($arResult);
?>

					<div class="card section cl">
						<div class="view left">


							<div class="photo">
							
								<?if($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=='да'):?>
									<img src="/src/spec.png" class="spec"/>
								<?elseif($arResult["PROPERTIES"]["NEWPRODUCT"]["VALUE"]=='да'):?>
									<img src="/src/noveltie.png" class="spec"/>
								<?elseif($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]=='да'):?>
									<img src="/src/leader.png" class="spec"/>
								<?elseif($arResult["PROPERTIES"]["BESTPRICE"]["VALUE"]=='да'):?>
									<img src="/src/bestprice.png" class="spec"/>
								<?endif;?>

								<ul class="card_image_slider">
									<li>
										<div class="wrap">
											<?if(is_array($arResult["DETAIL_PICTURE"])):?>
											<?
												$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>308, 'height'=>308), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
											?>
											<img src="<?=$file['src']?>" width="<?=$file['width']?>" height="<?=$file['height']?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>"/>
											<a class="fancybox" href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" title="<?=$arResult["NAME"]?>"></a>
											<?endif;?>
										</div>
									</li>
									<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):?>
										<?$file = CFile::ResizeImageGet($pic, array('width'=>308, 'height'=>308), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
										<li>
											<div class="wrap">
												<img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" />
												<a class="fancybox" href="<?=CFile::GetPath($pic);?>" title="<?=$arResult["NAME"]?>"></a>
											</div>
										</li>
									<?endforeach;?>
								</ul>
							</div>
							<div class="view-pager-wrapper">
								<div class="view-pager cl card_image_slider_nav">
									<?if(is_array($arResult["DETAIL_PICTURE"])):?>
									<?
										$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>78, 'height'=>78), BX_RESIZE_IMAGE_PROPORTIONAL, true);     
										$i=1;
									?>
									<a data-slide-index="0" href="javascript: void(0);" title="<?=$arResult["NAME"]?>"><img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" /></a>
									<?endif;?>
									<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):?>
										<?$file = CFile::ResizeImageGet($pic, array('width'=>78, 'height'=>78), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
										<a data-slide-index="<?=$i;?>" href="javascript: void(0);" title="<?=$arResult["NAME"]?>"><img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" /></a>
										<?$i++;?>
									<?endforeach;?>
								</div>
							</div>
						</div>
						<div class="cont_right">
							<h3><?=$arResult["NAME"]?></h3>
							<div class="descr">
								<p><?=$arResult["PREVIEW_TEXT"]?></p>
								<p>Точный срок поставки товара необходимо уточнить у менеджеров магазина.</p>
							</div>
							<div class="char brd">
								<ul>
									<?if($arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]!=''):?>
										<li>
											<span class="left">Производитель:</span>
											<span class="right"><?=$arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]?></span>
										</li>
									<?endif;?>
									<?$i=0;?>
									<?foreach($arResult["PROPERTIES"] as $prop):?>
									<?if(substr($prop["CODE"], 0, 4)=='PROP' && $prop["VALUE"]!='' && $i<4):?>
										<li>
											<span class="left"><?=$prop["NAME"]?>:</span>
											<span class="right"><?=$prop["VALUE"]?></span>
										</li>
										<?$i++;?>
									<?endif;?>
									<?endforeach;?>								
								</ul>
							</div>
							<div class="price-buy cl">
								<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<?if(is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):?>
									<div class="cl mod">
										<strong>Модификация:</strong> 
											<select name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" class="offersselect2">
												<?foreach($arResult["OFFERS"] as $arOffer):?>
													<option value="<?=$arOffer["ID"]?>"><?=$arOffer["NAME"]?></option>
												<?endforeach;?>
											</select>
									</div>
									<div class="price left">
											
											<?$first = 1;?>
											<?foreach($arResult["OFFERS"] as $arOffer):?>
												<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
													<?if($arPrice["CAN_ACCESS"]):?>
														<div<?=$first?'':' class="hide"';?> id="p<?=$arOffer["ID"];?>">
														<?$first=0;?>
														<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
															<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
														<?else:?>
															<?=$arPrice["PRINT_VALUE"]?>
														<?endif?>
														</div>
													<?endif;?>
												<?endforeach;?>	
											<?endforeach;?>
										<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									</div>
									<div class="buy right">
										<input type="submit" class="btn blue_btn"> name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CT_BCE_CATALOG_ADD")?>">
										<?/*<a href="#" title="">Купить</a>*/?>
									</div>
									
								<?else:?>
									<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arResult["ID"]?>">

									
									<div class="price left">
											<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
												<?if($arPrice["CAN_ACCESS"]):?>
													<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
														<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
													<?else:?><?=$arPrice["PRINT_VALUE"]?><?endif;?>
												<?endif;?>
											<?endforeach;?>
									</div>
									<div class="buy right">
										<input type="submit" class="blue_btn" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET";?>" value="<?echo GetMessage("CT_BCE_CATALOG_ADD")?>">
										<?/*<a href="#" title="">Купить</a>*/?>
									</div>
								
								<?endif;?>
								</form>
						</div>
						<div class="clearfix"></div>
						<div class="soc">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-yashareTheme="counter"></div> 
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="info section">
						<div class="tab-ctrl">
							<ul class="cl">
								<li class="tab-ctrl-item curr a"><a href="#t1" title=""><span>Краткая информация</span></a></li>
								<?if($arResult["PROPERTIES"]["TECH"]["VALUE"]):?><li class="tab-ctrl-item b"><a href="#t2" title=""><span>Технические характеристики</span></a></li><?endif;?>
								<?if($arResult["PROPERTIES"]["COMPL"]["VALUE"]):?><li class="tab-ctrl-item b"><a href="#t3" title=""><span>Комплектация</span></a></li><?endif;?>
								<?if(is_array($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])):?><li class="tab-ctrl-item c"><a href="#t4" title=""><span>Сопутствующие товары</span></a></li><?endif;?>
								<?/*if(is_array($arResult["PROPERTIES"]["ANALOGS"]["VALUE"])):?><li class="d last"><a href="#t5" title=""><span>Аналоги</span></a></li><?endif;*/?>
							</ul>
						</div>
						<div class="tab-cont">
							<div class="tab-cont-item" id="t1"><?=$arResult["DETAIL_TEXT"]?></div>
							<?if($arResult["PROPERTIES"]["TECH"]["VALUE"]):?>
								<div class="tab-cont-item" id="t2">
									<?=$arResult["PROPERTIES"]["TECH"]["~VALUE"]["TEXT"];?>
									<br/>
									<div class="char brd">
									<table width="100%">
									<?foreach($arResult["PROPERTIES"] as $prop):?>
										<?if(substr($prop["CODE"], 0, 4)=='PROP' && $prop["VALUE"]!=''):?>
											<tr>
												<td class="cell"><?=$prop["NAME"]?>:</td>
												<td class="cell"><?=$prop["VALUE"]?></td>
											</tr>
										<?endif;?>
									<?endforeach;?>	
									</table>
				
									</div>
								</div>
							<?endif;?>
							<?if($arResult["PROPERTIES"]["COMPL"]["VALUE"]):?><div class="tab-cont-item" id="t3"><?=$arResult["PROPERTIES"]["COMPL"]["~VALUE"]["TEXT"];?></div><?endif;?>
							<?if(is_array($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])):?>
								<div class="tab-cont-item" id="t4">
								<div class="prod-list4 section">
								<?foreach($arResult["RECOMMEND"] as $elem):?>
								<div class="item">
									<div class="wrap cl">								
										<div class="img left">
											<?if($elem["PIC_SRC"]!=''):?>
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>"><img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" title="<?=$elem["NAME"]?>" /></a>
											<?endif;?>
										</div>
										<div class="cont left cl">
											<div class="head">
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<div class="article">
													<span class="label">Артикул:</span> <?=$elem["PROPERTY_ARTNUMBER_VALUE"];?>
												</div>
												<?endif;?>
												<div class="link">
													<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="<?=$elem["NAME"]?>"><?=$elem["NAME"]?></a>
												</div>
											</div>
											<div class="cl">
												<div class="char left">
													<ul>
														<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
														<li>
															<span class="left">Артикул:</span>
															<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
														<li>
															<span class="left">Производитель:</span>
															<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_TEMPRANGE_VALUE"]!=''):?>
														<li>
															<span class="left">Температурный диапазон:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_TEMPRANGE_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_GAB_VALUE"]!=''):?>
														<li>
															<span class="left">Габаритные размеры:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_GAB_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_VALUE_VALUE"]!=''):?>
														<li>
															<span class="left">Объем:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VALUE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_COOLINGTYPE_VALUE"]!=''):?>
														<li>
															<span class="left">Тип охлаждения:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_COOLINGTYPE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_VOLTAGE_VALUE"]!=''):?>
														<li>
															<span class="left">Напряжение в сети:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VOLTAGE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_WEIGHT_VALUE"]!=''):?>
														<li>
															<span class="left">Вес:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_WEIGHT_VALUE"];?></span>
														</li>
														<?endif;?>	
														
													</ul>

													
												</div>
											</div>
										</div>
										<div class="price-buy left last">									
											<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
											<div class="buy"><a class="blue_btn" href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">В корзину</a></div>									
										</div>								
									</div>
								</div>																		
								<?endforeach;?>	
								</div>
								</div>
							<?endif;?>
							<?/*if(is_array($arResult["PROPERTIES"]["ANALOGS"]["VALUE"])):?>
								<div id="t5">
									<div class="prod-list4 section">
								<?foreach($arResult["ANALOGS"] as $elem):?>
								<div class="item">
									<div class="wrap cl">								
										<div class="img left">
											<?if($elem["PIC_SRC"]!=''):?>
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>"><img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" title="<?=$elem["NAME"]?>" /></a>
											<?endif;?>
										</div>
										<div class="cont left cl">
											<div class="head">
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<div class="article">
													<span class="label">Артикул:</span> <?=$elem["PROPERTY_ARTNUMBER_VALUE"];?>
												</div>
												<?endif;?>
												<div class="link">
													<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="<?=$elem["NAME"]?>"><?=$elem["NAME"]?></a>
												</div>
											</div>
											<div class="cl">
												<div class="char left">
													<ul>
														<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
														<li>
															<span class="left">Артикул:</span>
															<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
														<li>
															<span class="left">Производитель:</span>
															<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_TEMPRANGE_VALUE"]!=''):?>
														<li>
															<span class="left">Температурный диапазон:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_TEMPRANGE_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_GAB_VALUE"]!=''):?>
														<li>
															<span class="left">Габаритные размеры:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_GAB_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_VALUE_VALUE"]!=''):?>
														<li>
															<span class="left">Объем:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VALUE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_COOLINGTYPE_VALUE"]!=''):?>
														<li>
															<span class="left">Тип охлаждения:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_COOLINGTYPE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_VOLTAGE_VALUE"]!=''):?>
														<li>
															<span class="left">Напряжение в сети:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VOLTAGE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_WEIGHT_VALUE"]!=''):?>
														<li>
															<span class="left">Вес:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_WEIGHT_VALUE"];?></span>
														</li>
														<?endif;?>	
														
													</ul>

													
												</div>
											</div>
										</div>
										<div class="price-buy left last">									
											<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
											<div class="buy"><a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">Купить</a></div>									
										</div>								
									</div>
								</div>
								<?endforeach;?>	
								
								</div>
								</div>
							<?endif;*/?>
						</div>
					</div>
					<div class="section cl">
						<?if($arResult["PROPERTIES"]["DOCUMENTATION"]["VALUE"]!=''):?>
						<div class="docs left">
							<a href="<?=CFile::GetPath($arResult["PROPERTIES"]["DOCUMENTATION"]["VALUE"]);?>" title="Документация по продукту" target="_blank">Документация по продукту</a>
						</div>
						<?endif;?>
						<?if($arResult["PROPERTIES"]["INSTRUCTION"]["VALUE"]!=''):?>
						<div class="instruction right">
							<a href="<?=CFile::GetPath($arResult["PROPERTIES"]["INSTRUCTION"]["VALUE"]);?>" title="Инструкция по продукту" target="_blank">Инструкция по продукту</a>
						</div>
						<?endif;?>
					</div>
					
					
						<?/*if(count($_SESSION["SEEN"])>0):?>
						<h3>Просмотренные товары</h3>
						<div class="prod-list section">
							<div class="row cl last">
								<?$i=0;?>
								<?foreach(array_reverse($_SESSION["SEEN"]) as $elem):?>
									<div class="item left<?=$i==3?' last':''?>">
										<div class="img">
											<img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" />
										</div>
										<div class="link">
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title=""><?=$elem["NAME"]?></a>
										</div>
										<div class="char-list">
											<ul>
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<li>
													<span class="left">Артикул:</span>
													<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
												<li>
													<span class="left">Производитель:</span>
													<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_POWER_VALUE"]!=''):?>
												<li>
													<span class="left">Мощность:</span>
													<span class="right"><?=$elem["PROPERTY_POWER_VALUE"];?></span>
												</li>
												<?endif;?>
												
											</ul>
										</div>
										<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
										<div class="buy">
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">Купить</a>
										</div>
									</div>
									
									<?$i++;?>
									<?if($i>3) break;?>
									
								<?endforeach;?>				
								
							</div>
						</div>
						<?endif;*/?>
						
						
					
				
<?/*

<div class="catalog-element">
	<table width="100%" border="0" cellspacing="0" cellpadding="2">
		<tr>
		<?if(is_array($arResult["PREVIEW_PICTURE"]) || is_array($arResult["DETAIL_PICTURE"])):?>
			<td width="0%" valign="top">
				<?if(is_array($arResult["PREVIEW_PICTURE"]) && is_array($arResult["DETAIL_PICTURE"])):?>
					<img border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>" style="display:block;cursor:pointer;cursor: hand;" OnClick="document.getElementById('image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>').style.display='none';document.getElementById('image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>').style.display='block'" />
					<img border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>" style="display:none;cursor:pointer; cursor: hand;" OnClick="document.getElementById('image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>').style.display='none';document.getElementById('image_<?=$arResult["PREVIEW_PICTURE"]["ID"]?>').style.display='block'" />
				<?elseif(is_array($arResult["DETAIL_PICTURE"])):?>
					<img border="0" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>" height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
				<?elseif(is_array($arResult["PREVIEW_PICTURE"])):?>
					<img border="0" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" />
				<?endif?>
				<?if(count($arResult["MORE_PHOTO"])>0):?>
					<br /><a href="#more_photo"><?=GetMessage("CATALOG_MORE_PHOTO")?></a>
				<?endif;?>
			</td>
		<?endif;?>
			<td width="100%" valign="top">
				<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<?=$arProperty["NAME"]?>:<b>&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"])):
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					elseif($pid=="MANUAL"):
						?><a href="<?=$arProperty["VALUE"]?>"><?=GetMessage("CATALOG_DOWNLOAD")?></a><?
					else:
						echo $arProperty["DISPLAY_VALUE"];?>
					<?endif?></b><br />
				<?endforeach?>
			</td>
		</tr>
	</table>
	<?if(is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):?>
		<?foreach($arResult["OFFERS"] as $arOffer):?>
			<?foreach($arParams["OFFERS_FIELD_CODE"] as $field_code):?>
				<small><?echo GetMessage("IBLOCK_FIELD_".$field_code)?>:&nbsp;<?
						echo $arOffer[$field_code];?></small><br />
			<?endforeach;?>
			<?foreach($arOffer["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
				<small><?=$arProperty["NAME"]?>:&nbsp;<?
					if(is_array($arProperty["DISPLAY_VALUE"]))
						echo implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);
					else
						echo $arProperty["DISPLAY_VALUE"];?></small><br />
			<?endforeach?>
			<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
				<?if($arPrice["CAN_ACCESS"]):?>
					<p><?=$arResult["CAT_PRICES"][$code]["TITLE"];?>:&nbsp;&nbsp;
					<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
						<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
					<?else:?>
						<span class="catalog-price"><?=$arPrice["PRINT_VALUE"]?></span>
					<?endif?>
					</p>
				<?endif;?>
			<?endforeach;?>
			<p>
			<?if($arParams["DISPLAY_COMPARE"]):?>
				<noindex>
				<a href="<?echo $arOffer["COMPARE_URL"]?>" rel="nofollow"><?echo GetMessage("CT_BCE_CATALOG_COMPARE")?></a>&nbsp;
				</noindex>
			<?endif?>
			<?if($arOffer["CAN_BUY"]):?>
				<?if($arParams["USE_PRODUCT_QUANTITY"]):?>
					<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr valign="top">
							<td><?echo GetMessage("CT_BCE_QUANTITY")?>:</td>
							<td>
								<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
							</td>
						</tr>
					</table>
					<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
					<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arOffer["ID"]?>">
					<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">
					<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CT_BCE_CATALOG_ADD")?>">
					</form>
				<?else:?>
					<noindex>
					<a href="<?echo $arOffer["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
					&nbsp;<a href="<?echo $arOffer["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CT_BCE_CATALOG_ADD")?></a>
					</noindex>
				<?endif;?>
			<?elseif(count($arResult["CAT_PRICES"]) > 0):?>
				<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
				<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
					"NOTIFY_ID" => $arOffer['ID'],
					"NOTIFY_URL" => htmlspecialcharsback($arOffer["SUBSCRIBE_URL"]),
					"NOTIFY_USE_CAPTHA" => "N"
					),
					$component
				);?>
			<?endif?>
			</p>
		<?endforeach;?>
	<?else:?>
		<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
			<?if($arPrice["CAN_ACCESS"]):?>
				<p><?=$arResult["CAT_PRICES"][$code]["TITLE"];?>&nbsp;
				<?if($arParams["PRICE_VAT_SHOW_VALUE"] && ($arPrice["VATRATE_VALUE"] > 0)):?>
					<?if($arParams["PRICE_VAT_INCLUDE"]):?>
						(<?echo GetMessage("CATALOG_PRICE_VAT")?>)
					<?else:?>
						(<?echo GetMessage("CATALOG_PRICE_NOVAT")?>)
					<?endif?>
				<?endif;?>:&nbsp;
				<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
					<s><?=$arPrice["PRINT_VALUE"]?></s> <span class="catalog-price"><?=$arPrice["PRINT_DISCOUNT_VALUE"]?></span>
					<?if($arParams["PRICE_VAT_SHOW_VALUE"]):?><br />
						<?=GetMessage("CATALOG_VAT")?>:&nbsp;&nbsp;<span class="catalog-vat catalog-price"><?=$arPrice["DISCOUNT_VATRATE_VALUE"] > 0 ? $arPrice["PRINT_DISCOUNT_VATRATE_VALUE"] : GetMessage("CATALOG_NO_VAT")?></span>
					<?endif;?>
				<?else:?>
					<span class="catalog-price"><?=$arPrice["PRINT_VALUE"]?></span>
					<?if($arParams["PRICE_VAT_SHOW_VALUE"]):?><br />
						<?=GetMessage("CATALOG_VAT")?>:&nbsp;&nbsp;<span class="catalog-vat catalog-price"><?=$arPrice["VATRATE_VALUE"] > 0 ? $arPrice["PRINT_VATRATE_VALUE"] : GetMessage("CATALOG_NO_VAT")?></span>
					<?endif;?>
				<?endif?>
				</p>
			<?endif;?>
		<?endforeach;?>
		<?if(is_array($arResult["PRICE_MATRIX"])):?>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="data-table">
			<thead>
			<tr>
				<?if(count($arResult["PRICE_MATRIX"]["ROWS"]) >= 1 && ($arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)):?>
					<td><?= GetMessage("CATALOG_QUANTITY") ?></td>
				<?endif;?>
				<?foreach($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>
					<td><?= $arType["NAME_LANG"] ?></td>
				<?endforeach?>
			</tr>
			</thead>
			<?foreach ($arResult["PRICE_MATRIX"]["ROWS"] as $ind => $arQuantity):?>
			<tr>
				<?if(count($arResult["PRICE_MATRIX"]["ROWS"]) > 1 || count($arResult["PRICE_MATRIX"]["ROWS"]) == 1 && ($arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_FROM"] > 0 || $arResult["PRICE_MATRIX"]["ROWS"][0]["QUANTITY_TO"] > 0)):?>
					<th nowrap>
						<?if(IntVal($arQuantity["QUANTITY_FROM"]) > 0 && IntVal($arQuantity["QUANTITY_TO"]) > 0)
							echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_FROM_TO")));
						elseif(IntVal($arQuantity["QUANTITY_FROM"]) > 0)
							echo str_replace("#FROM#", $arQuantity["QUANTITY_FROM"], GetMessage("CATALOG_QUANTITY_FROM"));
						elseif(IntVal($arQuantity["QUANTITY_TO"]) > 0)
							echo str_replace("#TO#", $arQuantity["QUANTITY_TO"], GetMessage("CATALOG_QUANTITY_TO"));
						?>
					</th>
				<?endif;?>
				<?foreach($arResult["PRICE_MATRIX"]["COLS"] as $typeID => $arType):?>
					<td>
						<?if($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"] < $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"])
							echo '<s>'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"]).'</s> <span class="catalog-price">'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["DISCOUNT_PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
						else
							echo '<span class="catalog-price">'.FormatCurrency($arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["PRICE"], $arResult["PRICE_MATRIX"]["MATRIX"][$typeID][$ind]["CURRENCY"])."</span>";
						?>
					</td>
				<?endforeach?>
			</tr>
			<?endforeach?>
			</table>
			<?if($arParams["PRICE_VAT_SHOW_VALUE"]):?>
				<?if($arParams["PRICE_VAT_INCLUDE"]):?>
					<small><?=GetMessage('CATALOG_VAT_INCLUDED')?></small>
				<?else:?>
					<small><?=GetMessage('CATALOG_VAT_NOT_INCLUDED')?></small>
				<?endif?>
			<?endif;?><br />
		<?endif?>
		<?if($arResult["CAN_BUY"]):?>
			<?if($arParams["USE_PRODUCT_QUANTITY"] || count($arResult["PRODUCT_PROPERTIES"])):?>
				<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
				<table border="0" cellspacing="0" cellpadding="2">
				<?if($arParams["USE_PRODUCT_QUANTITY"]):?>
					<tr valign="top">
						<td><?echo GetMessage("CT_BCE_QUANTITY")?>:</td>
						<td>
							<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1" size="5">
						</td>
					</tr>
				<?endif;?>
				<?foreach($arResult["PRODUCT_PROPERTIES"] as $pid => $product_property):?>
					<tr valign="top">
						<td><?echo $arResult["PROPERTIES"][$pid]["NAME"]?>:</td>
						<td>
						<?if(
							$arResult["PROPERTIES"][$pid]["PROPERTY_TYPE"] == "L"
							&& $arResult["PROPERTIES"][$pid]["LIST_TYPE"] == "C"
						):?>
							<?foreach($product_property["VALUES"] as $k => $v):?>
								<label><input type="radio" name="<?echo $arParams["PRODUCT_PROPS_VARIABLE"]?>[<?echo $pid?>]" value="<?echo $k?>" <?if($k == $product_property["SELECTED"]) echo '"checked"'?>><?echo $v?></label><br>
							<?endforeach;?>
						<?else:?>
							<select name="<?echo $arParams["PRODUCT_PROPS_VARIABLE"]?>[<?echo $pid?>]">
								<?foreach($product_property["VALUES"] as $k => $v):?>
									<option value="<?echo $k?>" <?if($k == $product_property["SELECTED"]) echo '"selected"'?>><?echo $v?></option>
								<?endforeach;?>
							</select>
						<?endif;?>
						</td>
					</tr>
				<?endforeach;?>
				</table>
				<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
				<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arResult["ID"]?>">
				<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">
				<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD_TO_BASKET")?>">
				</form>
			<?else:?>
				<noindex>
				<a href="<?echo $arResult["BUY_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_BUY")?></a>
				&nbsp;<a href="<?echo $arResult["ADD_URL"]?>" rel="nofollow"><?echo GetMessage("CATALOG_ADD_TO_BASKET")?></a>
				</noindex>
			<?endif;?>
		<?elseif((count($arResult["PRICES"]) > 0) || is_array($arResult["PRICE_MATRIX"])):?>
			<?=GetMessage("CATALOG_NOT_AVAILABLE")?>
			<?$APPLICATION->IncludeComponent("bitrix:sale.notice.product", ".default", array(
				"NOTIFY_ID" => $arResult['ID'],
				"NOTIFY_PRODUCT_ID" => $arParams['PRODUCT_ID_VARIABLE'],
				"NOTIFY_ACTION" => $arParams['ACTION_VARIABLE'],
				"NOTIFY_URL" => htmlspecialcharsback($arResult["SUBSCRIBE_URL"]),
				"NOTIFY_USE_CAPTHA" => "N"
				),
				$component
			);?>
		<?endif?>
	<?endif?>
		<br />
	<?if($arResult["DETAIL_TEXT"]):?>
		<br /><?=$arResult["DETAIL_TEXT"]?><br />
	<?elseif($arResult["PREVIEW_TEXT"]):?>
		<br /><?=$arResult["PREVIEW_TEXT"]?><br />
	<?endif;?>
	<?if(count($arResult["LINKED_ELEMENTS"])>0):?>
		<br /><b><?=$arResult["LINKED_ELEMENTS"][0]["IBLOCK_NAME"]?>:</b>
		<ul>
	<?foreach($arResult["LINKED_ELEMENTS"] as $arElement):?>
		<li><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
	<?endforeach;?>
		</ul>
	<?endif?>
	<?
	// additional photos
	$LINE_ELEMENT_COUNT = 2; // number of elements in a row
	if(count($arResult["MORE_PHOTO"])>0):?>
		<a name="more_photo"></a>
		<?foreach($arResult["MORE_PHOTO"] as $PHOTO):?>
			<img border="0" src="<?=$PHOTO["SRC"]?>" width="<?=$PHOTO["WIDTH"]?>" height="<?=$PHOTO["HEIGHT"]?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" /><br />
		<?endforeach?>
	<?endif?>
	<?if(is_array($arResult["SECTION"])):?>
		<br /><a href="<?=$arResult["SECTION"]["SECTION_PAGE_URL"]?>"><?=GetMessage("CATALOG_BACK")?></a>
	<?endif?>*/?>
