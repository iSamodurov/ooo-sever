<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?global $islist;?>

<?
//echo '<pre>';
//print_r($arResult);
?>
<!--<h3><?= $arResult["NAME"] ?></h3> -->

<!--
						<div class="sort section">
							<div class="sort_param">Сортировка:</div>
							<?/*<div class="left">
							<?if($_SESSION["SORT"]=="SORT"):?>							
								по рейтингу
							<?else:?>
								<a href="?sort=rating" title="по рейтингу">по рейтингу</a>
							<?endif;?>
							</div>*/?>
							<div class="sort_param_value">
							<?if($_SESSION["DIR"]=="ASC"):?>							
								<a href="<?= $APPLICATION->GetCurPage();?>?sort=price&dir=desc" title="по цене">по цене по убыванию</a>
							<?else:?>
								<a href="<?= $APPLICATION->GetCurPage();?>?sort=price&dir=asc" title="по цене">по цене по возрастанию</a>
							<?endif;?>
							</div>
							<div class="left">
							<?/*if($_SESSION["SORT"]=="PROPERTY_BRAND_REF"):?>							
								по производителю
							<?else:?>
								<a href="?sort=brand" title="по производителю">по производителю</a>
							<?endif;*/?>
							</div>
						</div>
-->

<?if((CSite::InDir('/catalog/conditioners/') || CSite::InDir('/catalog/achome/') || CSite::InDir('/catalog/ac_poluprom/') || CSite::InDir('/catalog/multi_splitsystems/')) && !$islist):?>
<?/*<div class="sub-nav section">
	<ul>
		<?foreach($arResult["COND_TYPES"] as $type):?>
		<li<?=$type["ID"]==$_GET["dest"]?' class="curr"':'';?>><a href="/catalog/conditioners/?dest=<?=$type["ID"];?>&sub=Y" title="<?=$type["VALUE"];?>"><?=$type["VALUE"];?></a></li>
		<?endforeach;?>
	</ul>
</div>*/?>


<div class="calc section">
	<a href="/include/powcalc.php" title="Калькулятор расчета мощности" id="powcalc">Калькулятор расчета мощности</a>
</div>
<?endif;?>
<div class="prod-list2 section">

		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
		<div class="item" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
							
							<div class="wrap cl">
								
								<div class="img left">
									<?if($arElement["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=='да'):?>
										<img src="/src/spec.png" class="spec"/>
									<?endif;?>
									<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
									<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>									
									<?else:?>
									<br/>
									<?endif;?>
								</div>
								<div class="cont left cl">
									<div class="head">
										<?if($arElement["PROPERTIES"]["ARTNUMBER"]["VALUE"]!=''):?>
										<div class="article">
											<span class="label">Артикул:</span> <?=$arElement["PROPERTIES"]["ARTNUMBER"]["VALUE"];?>
										</div>
										<?endif;?>
										<div class="link">
											<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>"><?=$arElement["NAME"]?></a>
										</div>
									</div>
									<div class="cl">
										<div class="char left">
											<ul>
												<?if($arElement["PROPERTIES"]["MANUFACTURER"]["VALUE"]!=''):?>
												<li>
													<span class="left">Производитель:</span>
													<span class="right"><?=$arElement["PROPERTIES"]["MANUFACTURER"]["VALUE"]?></span>
												</li>
												<?endif;?>
												<?foreach($arElement["PROPERTIES"] as $prop):?>
												<?if(substr($prop["CODE"], 0, 4)=='PROP' && $prop["VALUE"]!=''):?>
												<li>
													<span class="left"><?=$prop["NAME"]?>:</span>
													<span class="right"><?=$prop["VALUE"]?></span>
												</li>
												<?endif;?>
												<?endforeach;?>
												<?if(is_array($arElement["OFFERS"]) && !empty($arElement["OFFERS"])):?>	
												<li class="mod">
												<span class="left">Модификация:</span>
												<span class="right">
												<select name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" class="offersselect">
													<?foreach($arElement["OFFERS"] as $arOffer):?>
													<option value="<?=$arOffer["ID"]?>"><?=$arOffer["NAME"]?></option>
													<?endforeach;?>
												</select>
												</span>
												</li>
												<?endif;?>
											</ul>
											
											
												<?/*<li>
													<span class="left">Мощн. обогрева, Вт:</span>
													<span class="right">3500</span>
												</li>
												<li>
													<span class="left">На помещение, м2:</span>
													<span class="right">35</span>
												</li>*/?>
											
										</div>
										
									</div>
								</div>
								<div class="price-buy left last">									
									<div class="price">
										<?if(!is_array($arElement["OFFERS"]) || empty($arElement["OFFERS"])):?>
											<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
												<?if($arPrice["CAN_ACCESS"]):?>
													<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
														<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
														<?CIBlockElement::SetPropertyValuesEx($arElement["ID"], 3, array("PRICE"=>$arPrice["DISCOUNT_VALUE"]));?>
													<?else:?><?=$arPrice["PRINT_VALUE"]?><?CIBlockElement::SetPropertyValuesEx($arElement["ID"], 3, array("PRICE"=>$arPrice["VALUE"]));?><?endif;?>
												<?endif;?>
											<?endforeach;?>
										<?else:?>
											<?$first = 1;?>
											<?foreach($arElement["OFFERS"] as $arOffer):?>
												<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
													<?if($arPrice["CAN_ACCESS"]):?>
														<div<?=$first?'':' class="hide"';?> id="p<?=$arOffer["ID"];?>">
														<?$first=0;?>
														<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
															<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
															<?CIBlockElement::SetPropertyValuesEx($arElement["ID"], 3, array("PRICE"=>$arPrice["DISCOUNT_VALUE"]));?>
														<?else:?>
															<?=$arPrice["PRINT_VALUE"]?>
															<?CIBlockElement::SetPropertyValuesEx($arElement["ID"], 3, array("PRICE"=>$arPrice["VALUE"]));?>
														<?endif?>
														</div>
													<?endif;?>
												<?endforeach;?>	
											<?endforeach;?>												
										<?endif;?>
									</div>
									<div class="buy">
										<input type="submit" class="main_blue_btn" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CATALOG_ADD")?>">
										<?/*<a href="#" title="">Купить</a>*/?>
									</div>

									<div class="count right">
											
												<fieldset>
													<a class="minus" href="#" title="">-</a>
													<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1">
													<a class="plus" href="#" title="">+</a>
													<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
													<?if(!is_array($arElement["OFFERS"]) || empty($arElement["OFFERS"])):?>
													<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
													<?endif;?>
													<?/*<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">*/?>
													
												</fieldset>
											
										</div>

								</div>								
							</div>
		
	
<div class="clearfix"></div>


		</div>
		</form>

		

		<?endforeach;?>




<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
