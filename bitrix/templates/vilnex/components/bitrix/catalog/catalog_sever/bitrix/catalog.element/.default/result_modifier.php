<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

/*if(!in_array($arResult["ID"], $_SESSION["SEEN_ID"])) $_SESSION["SEEN_ID"][]=$arResult["ID"];*/

//Составляем массив свойств
$props = array(
	"PROPERTY_MANUFACTURER",
	"PROPERTY_ARTNUMBER",
	"PROPERTY_PROP_TEMPRANGE",
	"PROPERTY_PROP_GAB",
	"PROPERTY_PROP_VALUE",
	"PROPERTY_PROP_COOLINGTYPE",
	"PROPERTY_PROP_VOLTAGE",
	"PROPERTY_PROP_WEIGHT"
	);
/*foreach($arResult["PROPERTIES"] as $code => $val)
{
	if(substr($code, 0, 4)=="PROP") $props[] = "PROPERTY_".$code;

}*/

//Выбираем рекомендуемые товары
$arSelect = array_merge($props, Array("ID", "NAME", "PREVIEW_PICTURE", "CATALOG_GROUP_1", "DETAIL_PAGE_URL"));
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y", "ID" => $arResult["PROPERTIES"]["RECOMMEND"]["VALUE"]);
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
$res->SetUrlTemplates("/catalog/#SECTION_CODE#/#ELEMENT_ID#/");
$i=0;
while($arFields = $res->GetNext())
{
	$arFields["PIC_SRC"] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
	$arResult["RECOMMEND"][]=$arFields;
	$i++;
	//echo $arFields["ID"];
	
}

//Выбираем аналоги

$arSelect = array_merge($props, Array("ID", "NAME", "PREVIEW_PICTURE", "CATALOG_GROUP_1", "DETAIL_PAGE_URL"));
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y", "ID" => $arResult["PROPERTIES"]["ANALOGS"]["VALUE"]);
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
$res->SetUrlTemplates("/catalog/#SECTION_CODE#/#ELEMENT_ID#/");
$i=0;
while($arFields = $res->GetNext())
{
	$arFields["PIC_SRC"] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
	$arResult["ANALOGS"][]=$arFields;
	$i++;
	//echo $arFields["ID"];
	
}

//Выбираем просмотренные товары

/*$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "PROPERTY_ARTNUMBER", "PROPERTY_MANUFACTURER", "PROPERTY_POWER", "CATALOG_GROUP_1", "DETAIL_PAGE_URL");
$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE"=>"Y", "ID" => $_SESSION["SEEN_ID"]);
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
$res->SetUrlTemplates("/catalog/#SECTION_CODE#/#ELEMENT_ID#/");
$i=0;
while($arFields = $res->GetNext())
{
	$arFields["PIC_SRC"] = CFile::GetPath($arFields["PREVIEW_PICTURE"]);
	if($i%2==0) $_SESSION["SEEN"][$arFields["ID"]]=$arFields;
	$i++;
	//echo $arFields["ID"];
	
}
*/





?>