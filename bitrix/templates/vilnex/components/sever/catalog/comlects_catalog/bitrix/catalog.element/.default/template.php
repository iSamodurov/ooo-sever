<?php
	//ini_set('display_errors', 1);
	//ini_set('display_startup_errors', 1);
	//error_reporting(E_ALL);
	//print_r($arResult['PRICE_MATRIX']['MATRIX'][1][0]['PRICE']);
?>



<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

					<div class="card section cl">


						<div class="view left">
							<div class="photo">
								<ul class="card_image_slider">

									<? if(!isset($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0])): ?>
									<li>
										<div class="wrap">
											<?if($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]=='да'):?>
												<img src="/src/spec.png" class="spec"/>
											<?elseif($arResult["PROPERTIES"]["NEWPRODUCT"]["VALUE"]=='да'):?>
												<img src="/src/noveltie.png" class="spec"/>
											<?elseif($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]=='да'):?>
												<img src="/src/leader.png" class="spec"/>
											<?elseif($arResult["PROPERTIES"]["BESTPRICE"]["VALUE"]=='да'):?>
												<img src="/src/bestprice.png" class="spec"/>
											<?endif;?>

											
											<?if(is_array($arResult["DETAIL_PICTURE"])):?>
											<?
												$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>308, 'height'=>308), BX_RESIZE_IMAGE_PROPORTIONAL, true);                
											?>
											<img src="<?=$file['src']?>" width="<?=$file['width']?>" height="<?=$file['height']?>" alt="<?=$arResult["NAME"]?>" title="<?=$arResult["NAME"]?>" id="image_<?=$arResult["DETAIL_PICTURE"]["ID"]?>"/>
											<a class="fancybox" href="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" title="<?=$arResult["NAME"]?>"></a>
											<?endif;?>


										</div>
									</li>
									<? endif ?>

									<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):?>
										<?$file = CFile::ResizeImageGet($pic, array('width'=>308, 'height'=>308), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
										<li>
											<div class="wrap">
												<img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" />
												<a class="fancybox" href="<?=CFile::GetPath($pic);?>" title="<?=$arResult["NAME"]?>"></a>
											</div>
										</li>
									<?endforeach;?>
								</ul>
							</div>
							<div class="view-pager-wrapper">
								<div class="view-pager cl card_image_slider_nav">

								<? if(!isset($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0])): ?>

									<?if(is_array($arResult["DETAIL_PICTURE"])):?>
									<?
										$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>78, 'height'=>78), BX_RESIZE_IMAGE_PROPORTIONAL, true);     
										$i=1;
									?>
									<a data-slide-index="0" href="javascript: void(0);" title="<?=$arResult["NAME"]?>"><img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" /></a>
									<?endif;?>

								<? endif; ?>


									<?foreach($arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $pic):?>
										<?$file = CFile::ResizeImageGet($pic, array('width'=>78, 'height'=>78), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
										<a data-slide-index="<?=$i;?>" href="javascript: void(0);" title="<?=$arResult["NAME"]?>"><img src="<?=$file['src']?>" alt="<?=$arResult["NAME"]?>" /></a>
										<?$i++;?>
									<?endforeach;?>
								</div>
							</div>
						</div>
						<div class="cont_right">
							<div class="descr">
								<p><?=$arResult["PREVIEW_TEXT"]?></p>
								<p>Точный срок поставки товара необходимо уточнить у менеджеров магазина.</p>
							</div>


							<div class="char brd">
								<ul>
									<?if($arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]!=''):?>
										<li>
											<span class="left">Производитель:</span>
											<span class="right"><?=$arResult["PROPERTIES"]["MANUFACTURER"]["VALUE"]?></span>
										</li>
									<?endif;?>
									<?$i=0;?>
									<?foreach($arResult["PROPERTIES"] as $prop):?>
									<?if(substr($prop["CODE"], 0, 4)=='PROP' && $prop["VALUE"]!='' && $i<4):?>
										<li>
											<span class="left"><?=$prop["NAME"]?>:</span>
											<span class="right"><?=$prop["VALUE"]?></span>
										</li>
										<?$i++;?>
									<?endif;?>
									<?endforeach;?>								
								</ul>
							</div>
							<div class="price-buy cl">
								<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
								<?if(is_array($arResult["OFFERS"]) && !empty($arResult["OFFERS"])):?>
									<div class="cl mod">
										<strong>Модификация:</strong> 
											<select name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" class="offersselect2">
												<?foreach($arResult["OFFERS"] as $arOffer):?>
													<option value="<?=$arOffer["ID"]?>"><?=$arOffer["NAME"]?></option>
												<?endforeach;?>
											</select>
									</div>
									<div class="price left">
											<?$first = 1;?>
											<?foreach($arResult["OFFERS"] as $arOffer):?>
												<?foreach($arOffer["PRICES"] as $code=>$arPrice):?>
													<?if($arPrice["CAN_ACCESS"]):?>
														<div<?=$first?'':' class="hide"';?> id="p<?=$arOffer["ID"];?>">
														<?$first=0;?>
														<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
															<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
														<?else:?>
															<?=$arPrice["PRINT_VALUE"]?>
														<?endif?>
														</div>
													<?endif;?>
												<?endforeach;?>	
											<?endforeach;?>
										<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									</div>
									<div class="buy right">
										<input type="submit" class="btn blue_btn"> name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="<?echo GetMessage("CT_BCE_CATALOG_ADD")?>">
										<?/*<a href="#" title="">Купить</a>*/?>
									</div>
									
								<?else:?>
									<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
									<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arResult["ID"]?>">

									
									<div class="price left">

										<b>Цена: <?php echo($arResult['PRICE_MATRIX']['MATRIX'][1][0]['PRICE']); ?> руб.</b>
										
											<?foreach($arResult["PRICES"] as $code=>$arPrice):?>
												<?if($arPrice["CAN_ACCESS"]):?>
													<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
														<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
													<?else:?><?=$arPrice["PRINT_VALUE"]?><?endif;?>
												<?endif;?>
											<?endforeach;?>
									</div>
									<div class="buy right">
										<input type="submit" class="blue_btn" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET";?>" value="<?echo GetMessage("CT_BCE_CATALOG_ADD")?>">
										<?/*<a href="#" title="">Купить</a>*/?>
									</div>
								
								<?endif;?>
								</form>
						</div>
						<div class="clearfix"></div>
						<div class="soc">
							<script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
							<div class="yashare-auto-init" data-yashareL10n="ru" data-yashareQuickServices="yaru,vkontakte,facebook,twitter,odnoklassniki,moimir,gplus" data-yashareTheme="counter"></div> 
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="info section">
						<div class="tab-ctrl">
							<ul class="cl">
								<li class="tab-ctrl-item curr a"><a href="#t1" title=""><span>Краткая информация</span></a></li>
								<?if($arResult["PROPERTIES"]["TECH"]["VALUE"]):?><li class="tab-ctrl-item b"><a href="#t2" title=""><span>Технические характеристики</span></a></li><?endif;?>
								<?if($arResult["PROPERTIES"]["COMPL"]["VALUE"]):?><li class="tab-ctrl-item b"><a href="#t3" title=""><span>Комплектация</span></a></li><?endif;?>
								<?if(is_array($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])):?><li class="tab-ctrl-item c"><a href="#t4" title=""><span>Сопутствующие товары</span></a></li><?endif;?>
								<?/*if(is_array($arResult["PROPERTIES"]["ANALOGS"]["VALUE"])):?><li class="d last"><a href="#t5" title=""><span>Аналоги</span></a></li><?endif;*/?>
							</ul>
						</div>
						<div class="tab-cont">
							<div class="tab-cont-item" id="t1"><?=$arResult["DETAIL_TEXT"]?></div>
							<?if($arResult["PROPERTIES"]["TECH"]["VALUE"]):?>
								<div class="tab-cont-item" id="t2">
									<?=$arResult["PROPERTIES"]["TECH"]["~VALUE"]["TEXT"];?>
									<br/>
									<div class="char brd">
									<table width="100%">
									<?foreach($arResult["PROPERTIES"] as $prop):?>
										<?if(substr($prop["CODE"], 0, 4)=='PROP' && $prop["VALUE"]!=''):?>
											<tr>
												<td class="cell"><?=$prop["NAME"]?>:</td>
												<td class="cell"><?=$prop["VALUE"]?></td>
											</tr>
										<?endif;?>
									<?endforeach;?>	
									</table>
				
									</div>
								</div>
							<?endif;?>
							<?if($arResult["PROPERTIES"]["COMPL"]["VALUE"]):?><div class="tab-cont-item" id="t3"><?=$arResult["PROPERTIES"]["COMPL"]["~VALUE"]["TEXT"];?></div><?endif;?>
							<?if(is_array($arResult["PROPERTIES"]["RECOMMEND"]["VALUE"])):?>
								<div class="tab-cont-item" id="t4">
								<div class="prod-list4 section">
								<?foreach($arResult["RECOMMEND"] as $elem):?>
								<div class="item">
									<div class="wrap cl">								
										<div class="img left">
											<?if($elem["PIC_SRC"]!=''):?>
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>"><img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" title="<?=$elem["NAME"]?>" /></a>
											<?endif;?>
										</div>
										<div class="cont left cl">
											<div class="head">
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<div class="article">
													<span class="label">Артикул:</span> <?=$elem["PROPERTY_ARTNUMBER_VALUE"];?>
												</div>
												<?endif;?>
												<div class="link">
													<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="<?=$elem["NAME"]?>"><?=$elem["NAME"]?></a>
												</div>
											</div>
											<div class="cl">
												<div class="char left">
													<ul>
														<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
														<li>
															<span class="left">Артикул:</span>
															<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
														<li>
															<span class="left">Производитель:</span>
															<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_TEMPRANGE_VALUE"]!=''):?>
														<li>
															<span class="left">Температурный диапазон:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_TEMPRANGE_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_GAB_VALUE"]!=''):?>
														<li>
															<span class="left">Габаритные размеры:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_GAB_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_VALUE_VALUE"]!=''):?>
														<li>
															<span class="left">Объем:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VALUE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_COOLINGTYPE_VALUE"]!=''):?>
														<li>
															<span class="left">Тип охлаждения:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_COOLINGTYPE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_VOLTAGE_VALUE"]!=''):?>
														<li>
															<span class="left">Напряжение в сети:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VOLTAGE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_WEIGHT_VALUE"]!=''):?>
														<li>
															<span class="left">Вес:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_WEIGHT_VALUE"];?></span>
														</li>
														<?endif;?>	
														
													</ul>

													
												</div>
											</div>
										</div>
										<div class="price-buy left last">									
											<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
											<div class="buy"><a class="blue_btn" href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">В корзину</a></div>									
										</div>								
									</div>
								</div>																		
								<?endforeach;?>	
								</div>
								</div>
							<?endif;?>

							<?if(is_array($arResult["PROPERTIES"]["ANALOGS"]["VALUE"])):?>
								<div id="t5">
									<div class="prod-list4 section">
								<?foreach($arResult["ANALOGS"] as $elem):?>
								<div class="item">
									<div class="wrap cl">								
										<div class="img left">
											<?if($elem["PIC_SRC"]!=''):?>
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>"><img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" title="<?=$elem["NAME"]?>" /></a>
											<?endif;?>
										</div>
										<div class="cont left cl">
											<div class="head">
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<div class="article">
													<span class="label">Артикул:</span> <?=$elem["PROPERTY_ARTNUMBER_VALUE"];?>
												</div>
												<?endif;?>
												<div class="link">
													<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="<?=$elem["NAME"]?>"><?=$elem["NAME"]?></a>
												</div>
											</div>
											<div class="cl">
												<div class="char left">
													<ul>
														<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
														<li>
															<span class="left">Артикул:</span>
															<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
														<li>
															<span class="left">Производитель:</span>
															<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_TEMPRANGE_VALUE"]!=''):?>
														<li>
															<span class="left">Температурный диапазон:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_TEMPRANGE_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_GAB_VALUE"]!=''):?>
														<li>
															<span class="left">Габаритные размеры:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_GAB_VALUE"];?></span>
														</li>
														<?endif;?>
														<?if($elem["PROPERTY_PROP_VALUE_VALUE"]!=''):?>
														<li>
															<span class="left">Объем:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VALUE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_COOLINGTYPE_VALUE"]!=''):?>
														<li>
															<span class="left">Тип охлаждения:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_COOLINGTYPE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_VOLTAGE_VALUE"]!=''):?>
														<li>
															<span class="left">Напряжение в сети:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_VOLTAGE_VALUE"];?></span>
														</li>
														<?endif;?>	
														<?if($elem["PROPERTY_PROP_WEIGHT_VALUE"]!=''):?>
														<li>
															<span class="left">Вес:</span>
															<span class="right"><?=$elem["PROPERTY_PROP_WEIGHT_VALUE"];?></span>
														</li>
														<?endif;?>	
														
													</ul>

													
												</div>
											</div>
										</div>
										<div class="price-buy left last">									
											<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
											<div class="buy"><a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">Купить</a></div>									
										</div>								
									</div>
								</div>
								<?endforeach;?>	
								
								</div>
								</div>
							<?endif;?>
						</div>
					</div>
					<div class="section cl">
						<?if($arResult["PROPERTIES"]["DOCUMENTATION"]["VALUE"]!=''):?>
						<div class="docs left">
							<a href="<?=CFile::GetPath($arResult["PROPERTIES"]["DOCUMENTATION"]["VALUE"]);?>" title="Документация по продукту" target="_blank">Документация по продукту</a>
						</div>
						<?endif;?>
						<?if($arResult["PROPERTIES"]["INSTRUCTION"]["VALUE"]!=''):?>
						<div class="instruction right">
							<a href="<?=CFile::GetPath($arResult["PROPERTIES"]["INSTRUCTION"]["VALUE"]);?>" title="Инструкция по продукту" target="_blank">Инструкция по продукту</a>
						</div>
						<?endif;?>
					</div>
					
					
						<?
						if(count($_SESSION["SEEN"])>0):?>
						<h3>Просмотренные товары</h3>
						<div class="prod-list section">
							<div class="row cl last">
								<?$i=0;?>
								<?foreach(array_reverse($_SESSION["SEEN"]) as $elem):?>
									<div class="item left<?=$i==3?' last':''?>">
										<div class="img">
											<img src="<?=$elem["PIC_SRC"]?>" alt="<?=$elem["NAME"]?>" />
										</div>
										<div class="link">
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title=""><?=$elem["NAME"]?></a>
										</div>
										<div class="char-list">
											<ul>
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<li>
													<span class="left">Артикул:</span>
													<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
												<li>
													<span class="left">Производитель:</span>
													<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_POWER_VALUE"]!=''):?>
												<li>
													<span class="left">Мощность:</span>
													<span class="right"><?=$elem["PROPERTY_POWER_VALUE"];?></span>
												</li>
												<?endif;?>
												
											</ul>
										</div>
										<?if($elem["CATALOG_PRICE_1"]!=0):?><div class="price"><?=CurrencyFormat($elem["CATALOG_PRICE_1"], $elem["CATALOG_CURRENCY_1"])?></div><?endif;?>
										<div class="buy">
											<a href="<?=$elem["DETAIL_PAGE_URL"]?>" title="">Купить</a>
										</div>
									</div>
									
									<?$i++;?>
									<?if($i>3) break;?>
									
								<?endforeach;?>				
								
							</div>
						</div>
						<?endif;?>
						
						

					
	




