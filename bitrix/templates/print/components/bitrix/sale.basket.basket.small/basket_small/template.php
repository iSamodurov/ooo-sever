<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="cart right">
<?
if ($arResult["READY"]=="Y" || $arResult["DELAY"]=="Y" || $arResult["NOTAVAIL"]=="Y" || $arResult["SUBSCRIBE"]=="Y"):
	$sum = 0;
	$quantaty = 0;
	foreach($arResult["ITEMS"] as $item)
	{
		$sum += $item["PRICE"]*$item["QUANTITY"];
		$quantaty += $item["QUANTITY"];
	}
?>

	<div class="count">Товаров: <a href="<?=$arParams["PATH_TO_BASKET"]?>"><?=$quantaty;?></a></div>
	<div class="total">На сумму: <b><?=$sum;?> р.</b></div>

<?else:?>
<div class="count">Ваша корзина пуста</div>
<div class="total"><br/></div>
<?endif;?>
</div>