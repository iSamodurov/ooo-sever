<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="prod-list3 section">
						<div class="titles">
							<table>
								<tr>
									<td class="a">Артикул</td>
									<td class="b">Название</td>
									<td class="c">Количество</td>
									<td class="d">Цена</td>
									<td class="e"></td>
								</tr>
							</table>
						</div>

		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>
		<form action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
			<div class="row" id="<?=$this->GetEditAreaId($arElement['ID']);?>">
							<table>
								<tr class="short">
									<td class="a article"><?=$arElement["PROPERTIES"]["CML2_ARTICLE"]["VALUE"];?></td>
									<td class="b link">
										<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>" name="<?=$arElement["ID"]?>"><?=$arElement["NAME"]?></a>
									</td>
									<td class="c count">
											<?if($arElement["CAN_BUY"]):?>
											<fieldset>
													<a class="minus" href="#" title=""></a>
													<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1">
													<a class="plus" href="#" title=""></a>
													<input type="hidden" name="<?echo $arParams["ACTION_VARIABLE"]?>" value="BUY">
													<?if(!is_array($arElement["OFFERS"]) || empty($arElement["OFFERS"])):?>
													<input type="hidden" name="<?echo $arParams["PRODUCT_ID_VARIABLE"]?>" value="<?echo $arElement["ID"]?>">
													<?endif;?>
													<?/*<input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."BUY"?>" value="<?echo GetMessage("CATALOG_BUY")?>">*/?>
													
											</fieldset>
											<?endif;?>
									</td>
									<td class="d price">
													<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
														<?if($arPrice["CAN_ACCESS"]):?>
															<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
																<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
															<?else:?><?=$arPrice["PRINT_VALUE"]?><?endif;?>
														<?endif;?>
													<?endforeach;?>
									</td>
									<td class="e buy">
										<?if($arElement["CAN_BUY"]):?><input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="Купить"><?endif;?>
									</td>
								</tr>
								<tr class="full">
									<td colspan="5">
										<table>
											<tr>
												<td class="a article"><?=$arElement["PROPERTIES"]["CML2_ARTICLE"]["VALUE"];?></td>
												<td class="b link">
													<strong><?=$arElement["NAME"]?></strong>
												</td>
												<td class="c count">
													<?if($arElement["CAN_BUY"]):?>
													<fieldset>
													<a class="minus" href="#" title=""></a>
													<input type="text" name="<?echo $arParams["PRODUCT_QUANTITY_VARIABLE"]?>" value="1">
													<a class="plus" href="#" title=""></a>													
													</fieldset>
													<?endif;?>
												</td>
												<td class="d price">
													<?foreach($arElement["PRICES"] as $code=>$arPrice):?>
														<?if($arPrice["CAN_ACCESS"]):?>
															<?if($arPrice["DISCOUNT_VALUE"] < $arPrice["VALUE"]):?>
																<s><?=$arPrice["PRINT_VALUE"]?></s> <?=$arPrice["PRINT_DISCOUNT_VALUE"]?>
															<?else:?><?=$arPrice["PRINT_VALUE"]?><?endif;?>
														<?endif;?>
													<?endforeach;?>
												</td>
												<td class="e buy">
													<?if($arElement["CAN_BUY"]):?><input type="submit" name="<?echo $arParams["ACTION_VARIABLE"]."ADD2BASKET"?>" value="Купить"><?endif;?>
												</td>
											</tr>
											<tr>
												<td class="img">
													<?if(is_array($arElement["PREVIEW_PICTURE"])):?>
													<a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arElement["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arElement["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arElement["NAME"]?>" title="<?=$arElement["NAME"]?>" /></a>
													<?endif;?>
												</td>
												<td class="char" colspan="4">
													<ul>
														<?if($arElement["PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"]!=''):?>
														<li>
															<span class="left">Производитель:</span>
															<span class="right"><?=$arElement["PROPERTIES"]["CML2_MANUFACTURER"]["VALUE"]?></span>
														</li>
														<?endif;?>
														<?if($arElement["PROPERTIES"]["POWER"]["VALUE"]!=''):?>
														<li>
															<span class="left">Мощн. охлаждения, Вт:</span>
															<span class="right"><?=$arElement["PROPERTIES"]["POWER"]["VALUE"];?></span>
														</li>
														<?endif;?>
													</ul>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>


		</div>
		</form>


		<?endforeach;?>




<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
