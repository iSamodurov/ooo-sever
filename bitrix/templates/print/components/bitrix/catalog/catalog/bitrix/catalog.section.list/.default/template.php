<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(count($arResult["SECTIONS"])>0):?>
<div class="cat-list section brd">

	
	<?
		$delta = $arResult["SECTIONS"][0]["DEPTH_LEVEL"]-1;
		foreach($arResult["SECTIONS"] as &$arSection)
		{
			$arSection["DEPTH_LEVEL"] = $arSection["DEPTH_LEVEL"] - $delta;
		}
	?>

	<?
	$prevdepth = 0;
	
	foreach($arResult["SECTIONS"] as $key => $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		
		if($arResult["SECTIONS"][$key+1]["DEPTH_LEVEL"]>$arSection["DEPTH_LEVEL"] && is_array($arResult["SECTIONS"][$key+1])) $isparent = 1;
		else $isparent = 0;
		
		if($arSection["DEPTH_LEVEL"]<$prevdepth && $arSection["DEPTH_LEVEL"]==2)
		{
		?>
				</li>
			</ul>
		<?
		}
		
		if($arSection["DEPTH_LEVEL"]<$prevdepth && $arSection["DEPTH_LEVEL"]==1)
		{
		?>
					</ul>
				</div>
			</div>
		</div>		
		<?
		}
		elseif($arSection["DEPTH_LEVEL"]==$prevdepth && $arSection["DEPTH_LEVEL"]==1)
		{
		?>
			</div>
		</div>	
		<?
		}
		
		if($arSection["DEPTH_LEVEL"]==1)
		{
		?>
		<div class="item cl">
			<div class="img left">
				<div class="wrap">
					<?if($arSection["PICTURE"]["SRC"]!=''):?>
					<a href="<?=$arSection["SECTION_PAGE_URL"]?>"><img alt="<?=$arSection["NAME"]?>" src="<?=$arSection["PICTURE"]["SRC"]?>"></a>
					
					<?endif;?>
				</div>
			</div>
			<div class="cont right"<?=$delta>0?' style="width: 550px;"':''?>>
				<?if($isparent):?>
				<a class="link" href="<?=$arSection["SECTION_PAGE_URL"]?>" title="<?=$arSection["NAME"]?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>"><?=$arSection["NAME"]?></a>
				<div class="sub">					
					<ul>
				<?else:?>
					<a class="link" href="<?=$arSection["SECTION_PAGE_URL"]?>" title="<?=$arSection["NAME"]?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>"><?=$arSection["NAME"]?></a>
				<?endif;?>
		<?
		}
		else
		{
		?>
			<?if($isparent):?>
				<li <?=$isparent?' class="parent"':''?>id="<?=$this->GetEditAreaId($arSection['ID']);?>"><a href="<?=$arSection["SECTION_PAGE_URL"]?>" title="<?=$arSection["NAME"]?>"><?=$arSection["NAME"]?></a>
					<ul<?=$delta>0 && $arSection["DEPTH_LEVEL"]==2?' style="width: 550px;"':''?>>
			<?else:?>
				<li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><a href="<?=$arSection["SECTION_PAGE_URL"]?>" title="<?=$arSection["NAME"]?>"><?=$arSection["NAME"]?></a></li>	
			<?endif;?>
		<?
		}
		
		$prevdepth = $arSection["DEPTH_LEVEL"];
		
	}
	
	if($prevdepth==3)
	{
	?>
						</ul></ul>
	<?
	}
	elseif($prevdepth==2)
	{
	?>
						</ul>
	<?
	}
	?>
	
					
				</div>
			</div>
		</div>
</div>
<?endif;?>