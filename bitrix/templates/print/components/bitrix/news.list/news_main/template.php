<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
			<div class="news section brd">
				<div class="head cl">
					<h3 class="left">Новости</h3>
					<a class="right" href="/about/news/" title="">Архив новостей</a>
				</div>
				<div class="cont">
					<ul class="cl">

					<?foreach($arResult["ITEMS"] as $key => $arItem):?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<li id="<?=$this->GetEditAreaId($arItem['ID']);?>"<?if($key==count($arResult["ITEMS"])-1):?> class="last"<?endif;?>>
							<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
							<div class="img left">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img alt="<?echo $arItem["NAME"]?>" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a>
							</div>
							<?endif;?>
							<div class="cont right">
								<div class="date"><?=FormatDate("d F Y", MakeTimeStamp($arItem["DISPLAY_ACTIVE_FROM"]))?></div>
								<a title="<?echo $arItem["NAME"]?>" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?echo $arItem["NAME"]?></a>
								<p><?echo $arItem["PREVIEW_TEXT"];?></p>
							</div>
						</li>
					<?endforeach;?>
					</ul>
				</div>
			</div>
