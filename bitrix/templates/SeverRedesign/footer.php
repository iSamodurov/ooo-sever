<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

    <section class="footer">
        <div class="container">

            <?$APPLICATION->IncludeComponent("bitrix:menu", "footerMenu", Array(
                "ROOT_MENU_TYPE" => "bottomNew", // Тип меню для первого уровня
                "MAX_LEVEL" => "1", // Уровень вложенности меню
                "CHILD_MENU_TYPE" => "bottomNew", // Тип меню для остальных уровней
                "USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N", // Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
                "MENU_CACHE_TYPE" => "N", // Тип кеширования
                "MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
            ), false);
            ?>
            
            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => SITE_TEMPLATE_PATH . "/include/footer-contacts.php"
                ));
            ?>

            <div class="footer-right">
                <div class="footer-contacts">
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => SITE_TEMPLATE_PATH . "/include/footer-phone.php"
                    ));
                    ?>
                    <a href="javascript:;" class="footer-contacts__callback showModal" data-modalId="#modal_form">Перезвоните мне</a>
                </div>
                <a href="/" class="logo">
                    <img src="<?=SITE_TEMPLATE_PATH."/img/logo.png"?>" alt="sever33 logo">
                </a>
                <div class="footer-copy">
                    <p> &copy; Компания &laquo;Север&raquo;, 2006 - <?=date('Y')?> г.</p>
                    <p>
                        Профессиональная оргово-холодильная компания <br>
                        в сфере холодильного оборудования <br>
                        и климатической техники
                    </p>
                </div>
            </div>

        </div>
    </section>

<!--   --------- MODALS  WINDOWS ---------  -->
<div style="display: none;" id="modal_form" class="modal-form">
    <h2 class="modal_title">Оставьте заявку</h2>
    <p class="modal_subtitle">и мы перезвоним вам в течении 10 минут</p>
    <form action="/php/submit.php" class="orderForm">
        <input type="text" class="form-control" name="name" placeholder="Ваше Имя:"> <br>
        <input type="tel" class="form-control" name="phone" placeholder="Введите телефон:"> <br>
        <input type="email" class="form-control" name="email" placeholder="Введите E-mail:"> <br>
        <button type="submit" class="btn btn-block modal_submit">Оставить заявку</button>

        <div class="checkboxes_container">
            <div class="checkboxes_item">
                <label>
                    <input type="checkbox" name="personalAgreement" checked required>
                    <span class="label_text">
                        Я даю свое согласие на обработку персональных данных и соглашаюсь
                        с условиями и политикой конфиденциальности
                    </span>
                </label>
            </div>

            <div class="checkboxes_item">
                <label>
                    <input type="checkbox" checked>
                    <span class="label_text">
                        Я хочу получать Email-письма о мероприятиях и/или иных услугах
                    </span>
                </label>
            </div>
        </div>

    </form>
</div>


<div style="display: none;" id="modal_auth" class="modal-form">
    <?$APPLICATION->IncludeComponent("bitrix:system.auth.form", "", Array(
            "REGISTER_URL" => "",
            "FORGOT_PASSWORD_URL" => "",
            "PROFILE_URL" => "/personal/profile/",
            "SHOW_ERRORS" => "Y"
        )
    );?>
</div>

<div style="display: none;" id="modal_reg" class="modal-form">
    <h2 class="modal_title">Регистрация</h2>
    <?$APPLICATION->IncludeComponent("bitrix:main.register","",Array(
            "USER_PROPERTY_NAME" => "",
            "SEF_MODE" => "Y",
            "SHOW_FIELDS" => Array(),
            "REQUIRED_FIELDS" => Array(),
            "AUTH" => "Y",
            "USE_BACKURL" => "Y",
            "SUCCESS_PAGE" => "",
            "SET_TITLE" => "N",
            "USER_PROPERTY" => Array(),
            "SEF_FOLDER" => "/",
            "VARIABLE_ALIASES" => Array()
        )
    );?>
</div>

    <?
    if ($bScriptInFooter) {
        $APPLICATION->ShowHeadStrings();
        $APPLICATION->ShowHeadScripts();
    }
    ?>
</body>
</html>