<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$bScriptInFooter = !$USER->isAdmin(); //скрипты вниз
?>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<base href="/">
	<?
	// meta
	$APPLICATION->AddHeadString('<meta name="viewport" content="user-scalable=yes, width=1100">');
	//css
	$APPLICATION->SetAdditionalCSS("https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.22/css/uikit.min.css");
	$APPLICATION->SetAdditionalCSS("https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/vendor/ion.rangeSlider.skinFlat.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/vendor/jquery.fancybox.css");
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/vendor/ion.rangeSlider.css"); 

	//BLOCKS удалить потом
//	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/style/blocks/userbar.css");
//	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "header.css");
//	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/style/blocks/navigation.css");

	//js
	$APPLICATION->AddHeadScript("https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js");
	$APPLICATION->AddHeadScript("https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.22/js/uikit.min.js");
	$APPLICATION->AddHeadScript("https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.22/js/uikit-icons.min.js");
	$APPLICATION->AddHeadScript("//api-maps.yandex.ru/2.1/?lang=ru_RU");

	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor/ion.rangeSlider.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor/jquery.singlePageNav.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor/jquery.fancybox.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor/jquery.validate.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/vendor/slick.min.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/main.js");
	?>
	<?//$APPLICATION->ShowHead();?>
	<?$APPLICATION->ShowCSS();
		if(!$bScriptInFooter){
			$APPLICATION->ShowHeadStrings();
			$APPLICATION->ShowHeadScripts();
		}
	?>
	<title><?$APPLICATION->ShowTitle()?></title>
	<?
		$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH . "/include/topScripts.php"
		));
	?>
</head>
<body class="<?=$APPLICATION->GetCurPage()=="/"?"homepage":"vlx-minheight"?>">
<?$APPLICATION->ShowPanel()?>
<section class="userbar">
	<div class="container">
		<ul class="userbar__list">
			<?
			$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/header-workList.php"
			));
			?>
		</ul>

			<?global $USER; ?>
			<?if (!$USER->IsAuthorized()){?>
				<span class="header-auth">
					<span>
						<a href="javascript:;" data-modalId="#modal_auth" class="showModal header-auth-link">Вход</a>
					</span>/
					<span>
						<a href="javascript:;" data-modalId="#modal_reg" class="showModal header-auth-link">Регистрация</a>
					</span>
				</span>
			<?}else{
				$name = $USER->GetFirstName();
				if (!strlen($name)) {
					$name = $USER->GetLogin();
				};
				?>
				<div class="userbar-profile">
					<span uk-icon="icon: user"></span>
					<div class="userbar-profile__name"><?=$name?></div>
					<span uk-icon="icon: chevron-down"></span>
					<div class="userbar__nav">
						<ul>
							<li><a href="/personal/profile/"><span uk-icon="icon: home"></span> Личный кабинет</a></li>
							<li><a href=""><span uk-icon="icon: tag"></span> Мои заказы</a></li>
							<li><a href="/personal/cart/"><span uk-icon="icon: cart"></span> Корзина</a></li>
							<li><a href="javascript:;" class="logout userbar__nav_red"><span uk-icon="icon: sign-out"></span> Выход</a></li>
						</ul>
					</div>
				</div>
			<?}?>
		
		<div class="clearfix"></div>
	</div>
</section>



<header class="header">
	<div class="container">
		<a href="/" class="logo">
			<img src="<?=SITE_TEMPLATE_PATH."/img/logo.png"?>" alt="Logo" class="logo__img">
		</a>

		<?$APPLICATION->IncludeComponent("bitrix:menu", "topMenu", Array(
			"ROOT_MENU_TYPE" => "topNew", // Тип меню для первого уровня
			"MAX_LEVEL" => "1", // Уровень вложенности меню
			"CHILD_MENU_TYPE" => "topNew", // Тип меню для остальных уровней
			"USE_EXT" => "N", // Подключать файлы с именами вида .тип_меню.menu_ext.php
			"DELAY" => "N", // Откладывать выполнение шаблона меню
			"ALLOW_MULTI_SELECT" => "N", // Разрешить несколько активных пунктов одновременно
			"MENU_CACHE_TYPE" => "N", // Тип кеширования
			"MENU_CACHE_TIME" => "3600", // Время кеширования (сек.)
			"MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
			"MENU_CACHE_GET_VARS" => "", // Значимые переменные запроса
		), false);
		?>

		<?/*$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "basket_small", Array(
			"PATH_TO_BASKET" => "/personal/cart/",  // Страница корзины
			"PATH_TO_ORDER" => "/personal/order/",  // Страница оформления заказа
			"SHOW_DELAY" => "N",  // Показывать отложенные товары
			"SHOW_NOTAVAIL" => "N",  // Показывать товары, недоступные для покупки
			"SHOW_SUBSCRIBE" => "N",  // Показывать товары, на которые подписан покупатель
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
		),
			false
		);*/?>
		
		<div class="header-contacts">
			<?
			$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
				"AREA_FILE_SHOW" => "file",
				"PATH" => SITE_TEMPLATE_PATH . "/include/header-contacts.php"
			));
			?>
			<a href="javascript:;" class="header-contacts__callback showModal" data-modalId="#modal_form">Перезвоните мне</a>
		</div>

	</div>
</header>
<nav class="navigation">
	<?$APPLICATION->IncludeComponent("bitrix:menu", "catalogMenu", array(
		"ROOT_MENU_TYPE" => "catalog",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "3",
		"CHILD_MENU_TYPE" => "catalog",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N"
		),
		false
	);?>

	<?$APPLICATION->IncludeComponent(
		"bitrix:main.include",
		"",
		Array(
			"AREA_FILE_SHOW" => "file",
			"PATH" => SITE_TEMPLATE_PATH."/include/search.php"
		)
	);?>

	<div class="clearfix"></div>
</nav>