<div class="footer_col footer-contacts">
    <b class="footer__title">Адреса и контакты</b>
    <figure class="footer-contacts__item">
        <b>Адрес</b>
        <p>
            г. Владимир, ул. Куйбышева, <br>
            66-Б (Центральный офис)</p>
    </figure>
    <figure class="footer-contacts__item">
        <b>Телефоны</b>
        <p>+7 (4922) 37-31-75</p>
        <p>+7 (4922) 377-277</p>
    </figure>
    <figure class="footer-contacts__item">
        <b>Почта</b>
        <p>info@sever33.ru</p>
    </figure>
</div>