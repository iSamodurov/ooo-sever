<a href="mailto:info@sever33.ru" class="header-contacts__email">
    <!--<span uk-icon="icon: mail"></span>-->
    <i class="fa fa-envelope-o" aria-hidden="true"></i>
    info@sever33.ru
</a>
<a href="tel:+74922377277" class="header-contacts__tel">
    <i class="fa fa-mobile" aria-hidden="true"></i>
    +7 (4922) 377-277
</a>