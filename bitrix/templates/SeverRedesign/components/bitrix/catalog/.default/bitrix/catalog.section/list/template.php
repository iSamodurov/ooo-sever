<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
?>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
		"START_FROM" => "0",
		"PATH" => "/catalog/",
		"SITE_ID" => "s1"
	)
);?>

<div class="right-side right-side_size_catalog">


	<div class="content">

		<div class="content-head">

			<div class="clearfix"></div>
			<h3 class="content-head__title"><?=$arResult["NAME"]?></h3>

			<div class="content-head__tools">
				<label> Вид: </label>
				<div class="uk-button-group catalog-view-toggler" data-view="block">
					<a href="<?=$APPLICATION->GetCurPageParam("catalogView=blocks", array("catalogView"))?>" class="btn-toggle" title="Вид плиткой" data-view="block" uk-tooltip>
						<span uk-icon="icon: grid"></span>
					</a>
					<a href="<?=$APPLICATION->GetCurPageParam("catalogView=list", array("catalogView"))?>" class="btn-toggle btn-toggle_active" data-view="list" title="Вид списком" uk-tooltip>
						<span uk-icon="icon: list"></span>
					</a>
				</div>
			</div>

			<div class="clearfix"></div>
		</div>

		<div class="content-body">
			<table class="uk-table uk-table-striped catalog-list">
				<thead class="table-head">
				<tr>
					<th></th>
					<th>Код</th>
					<th>
						<a href="" class="uk-button uk-button-text" uk-tooltip="" aria-expanded="false">
							Наименование <span uk-icon="icon: triangle-down" class="uk-icon"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" icon="triangle-down" ratio="1"><polygon points="5 7 15 7 10 12"></polygon></svg></span>
						</a>
					</th>
					<th>Бренд</th>
					<th>Цена</th>
					<th>Кол-во</th>
					<th></th>
				</tr>
				</thead>
				<tbody>
				<?foreach($arResult["ITEMS"] as $item){?>
				<tr>
					<td>
						<div class="product__img">
							<img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" alt="">
						</div>
					</td>
					<td><?=$item["ID"]?></td>
					<td><?=$item["NAME"]?></td>
					<td><?=$item["PROPERTIES"]["MANUFACTURER"]["VALUE"]?></td>
					<td><?=CurrencyFormat($item["PROPERTIES"]["PRICE"]["VALUE"], "RUB")?></td>
					<td>
						<div class="product-count">
							<button class="product-count__minus">
								<i class="fa fa-chevron-left"></i>
							</button>
							<input class="product-count__input" type="text" value="1">
							<button class="product-count__plus">
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</td>
					<td>
						<button class="btn add-to-cart" uk-tooltip="" aria-expanded="false">
							<span uk-icon="icon: cart" class="uk-icon"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" icon="cart" ratio="1"><circle cx="7.3" cy="17.3" r="1.4"></circle><circle cx="13.3" cy="17.3" r="1.4"></circle><polyline fill="none" stroke="#000" points="0 2 3.2 4 5.3 12.5 16 12.5 18 6.5 8 6.5"></polyline></svg></span>
							В корзину
						</button>
					</td>
				</tr>
				<?}?>
				</tbody>
			</table>

		</div>
		<div class="content-footer">

			<button class="btn btn-white" onclick="window.print()">
				<i class="fa fa-print"></i>
				Распечатать
			</button>

			<?
			if ($arParams["DISPLAY_BOTTOM_PAGER"])
			{
				?><? echo $arResult["NAV_STRING"]; ?><?
			}
			?>

			<div class="catalog-perpage">
				<label>
					Товаров на страницу:
					<select class="uk-select uk-form-small catalog-perpage__select">
						<?foreach($arResult["elemsQuantity"] as $elemQua){?>
							<option <?=$arParams["elementsQuantity"]==$elemQua?"selected":""?> data-href="<?=$APPLICATION->GetCurPageParam("elementsQuantity=".$elemQua, array("elementsQuantity"))?>"><?=$elemQua?></option>
						<?}?>
					</select>
				</label>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>