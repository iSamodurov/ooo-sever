<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$desc = $arResult["SECTION"]["DESCRIPTION"];
$sectionCnt = count($arResult["SECTIONS"]);
?>
<?if($arResult["SECTION"]["DESCRIPTION"]){?>
	<div class="expandet_text">
		<?=$arResult["SECTION"]["DESCRIPTION"]?>
	<div class="expandet_text__action"><button class="expandet_text__btn">Читать весь текст</button></div></div>
<?}?>
<?if($sectionCnt>0){?>
	<div class="cat-list section brd">
		<?foreach($arResult["SECTIONS"] as $section){?>
			<div class="item cl">
				<div class="img">
					<a href="<?=$section["SECTION_PAGE_URL"]?>">
						<img alt="<?=$section["PICTURE"]["ALT"]?>" src="<?=$section["PICTURE"]["SRC"]?>">
					</a>
				</div>
				<div class="cont">
					<a class="link" href="<?=$section["SECTION_PAGE_URL"]?>" title="<?=$section["NAME"]?>" id="<?=$section["ID"]?>"><?=$section["NAME"]?></a>
				</div>
			</div>
		<?}
		$emptyBlocks = 4 - $sectionCnt%4;
		$it = 0;
		while ($it++<$emptyBlocks){?>
			<div class="item cl empty_item"></div>
		<?}?>
	</div>
<?}?>

