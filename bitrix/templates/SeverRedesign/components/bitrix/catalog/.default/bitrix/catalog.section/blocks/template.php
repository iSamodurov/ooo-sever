<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
/** @global CDatabase $DB */
?>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","",Array(
		"START_FROM" => "0",
		"PATH" => "/catalog/",
		"SITE_ID" => "s1"
	)
);?>

<div class="right-side right-side_size_catalog">
	<div class="content content_theme_grid">
		<div class="content-head">

			<div class="clearfix"></div>
			<h3 class="content-head__title"><?=$arResult["NAME"]?></h3>
			<div class="content-head__tools">
				<label> Вид: </label>
				<div class="uk-button-group catalog-view-toggler">
					<a href="<?=$APPLICATION->GetCurPageParam("catalogView=blocks", array("catalogView"))?>" class="btn-toggle" title="Вид плиткой" data-view="block" uk-tooltip>
						<span uk-icon="icon: grid"></span>
					</a>
					<a href="<?=$APPLICATION->GetCurPageParam("catalogView=list", array("catalogView"))?>" class="btn-toggle btn-toggle_active" data-view="list" title="Вид списком" uk-tooltip>
						<span uk-icon="icon: list"></span>
					</a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>


		<div class="content-body uk-flex uk-flex-wrap uk-flex-around">

		<?foreach($arResult["ITEMS"] as $item){?>
			<div class="product-tile">
				<div class="product-tile__image">
					<img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>"
						 alt="">
				</div>
				<div class="product-tile__name">
					<h4><?=$item["NAME"]?></h4>
				</div>
				<div class="product-tile__price">
					<?=CurrencyFormat($item["PROPERTIES"]["PRICE"]["VALUE"], "RUB")?>
				</div>


				<div class="full-tile">
					<div class="product-tile__image">
						<a href="<?=$item["DETAIL_PAGE_URL"]?>"><img src="<?=$item["PREVIEW_PICTURE"]["SRC"]?>" alt=""> </a>
					</div>
					<div class="product-tile__name">
						<a href="<?=$item["DETAIL_PAGE_URL"]?>"><h4><?=$item["NAME"]?></h4></a>
					</div>
					<div class="product-tile__description ">
						<?$propsCnt = 0?>
						<?foreach($item["PROPERTIES"] as $kProp => $prop){?>
							<?if($kProp!="PRICE" && $prop["VALUE"] && $propsCnt<$arParams["MAX_PROPS_COUNT"]){?>
								<div class="product-param">
									<div class="product-param__name"><?=$prop["NAME"]?>:</div>
									<div class="product-param__value"><?=$prop["VALUE"]?></div>
								</div>
								<?$propsCnt++;?>
							<?}?>
						<?}?>
					</div>
					<div class="product-tile__price">
						<?=CurrencyFormat($item["PROPERTIES"]["PRICE"]["VALUE"], "RUB")?>
					</div>
				</div>
			</div>
		<?}
		$emptyBlocks = 4 - count($arResult["ELEMENTS"])%4;
		$it = 0;
		while ($it++<$emptyBlocks){?>
			<div class="empty-product-tile"></div>
		<?}?>
		</div>



		<div class="content-footer">
			<button class="btn btn-white" onclick="window.print()">
				<i class="fa fa-print"></i>
				Распечатать
			</button>

			<?
			if ($arParams["DISPLAY_BOTTOM_PAGER"])
			{
				?><? echo $arResult["NAV_STRING"]; ?><?
			}
			?>

			<div class="catalog-perpage">
				<label>
					Товаров на страницу:
					<select class="uk-select uk-form-small catalog-perpage__select">
						<?foreach($arResult["elemsQuantity"] as $elemQua){?>
							<option <?=$arParams["elementsQuantity"]==$elemQua?"selected":""?> data-href="<?=$APPLICATION->GetCurPageParam("elementsQuantity=".$elemQua, array("elementsQuantity"))?>"><?=$elemQua?></option>
						<?}?>
					</select>
				</label>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>