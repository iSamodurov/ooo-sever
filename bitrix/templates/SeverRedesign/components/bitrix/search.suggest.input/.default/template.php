<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array("ajax"));
?>
<?if(false){?>
<script type="text/javascript">
if (!window.oObject || typeof oObject != "object")
	window.oObject = {};

window.<?= $arResult["ID"]?>_CheckThis = document.<?= $arResult["ID"]?>_CheckThis = function(oObj)
{
try
{
if(SuggestLoaded)
{
if (typeof window.oObject[oObj.id] != 'object')
window.oObject[oObj.id] = new JsSuggest(oObj, '<?echo $arResult["ADDITIONAL_VALUES"]?>');
return;
}
else
{
setTimeout(<?echo $arResult["ID"]?>_CheckThis(oObj), 10);
}
}
catch(e)
{
setTimeout(<?echo $arResult["ID"]?>_CheckThis(oObj), 10);
}
}
</script>
<IFRAME style="width:0px; height:0px; border: 0px;" src="javascript:''" name="<?echo $arResult["ID"]?>_div_frame" id="<?echo $arResult["ID"]?>_div_frame"></IFRAME>
	<input <?if($arParams["INPUT_SIZE"] > 0):?> size="<?echo $arParams["INPUT_SIZE"]?>"<?endif?> name="<?echo $arParams["NAME"]?>" id="<?echo $arResult["ID"]?>" value="<?echo $arParams["VALUE"]?>" class="search-suggest" type="text" autocomplete="off" onfocus="<?echo $arResult["ID"]?>_CheckThis(this);" />
<?}?>
<div class="navigation-search">
	<form action="/search/">
		<div class="uk-inline">
			<span class="uk-form-icon" uk-icon="icon: search"></span>
			<input class="uk-input navigation-search__input" type="text" name="q" placeholder="<?=$_GET["q"]?$_GET["q"]:"Поиск по каталогу&hellip;"?>">
		</div>
		<button type="submit" class="navigation-search__submit">Выполнить поиск</button>
	</form>
</div>