$(document).ready(function(){
    $(".expandet_text__action").on("click",function(){
        var elem = $(this);
        var parent = elem.parent();
        var active = elem.hasClass("active");
        if(active){
            parent.removeClass("expandet_text__showed");
        }else{
            parent.addClass("expandet_text__showed");
        }
        elem.toggleClass('active');
    });
});