<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$sum = 0;
$quantaty = 0;
if ($arResult["READY"]=="Y" || $arResult["DELAY"]=="Y" || $arResult["NOTAVAIL"]=="Y" || $arResult["SUBSCRIBE"]=="Y") {
	foreach ($arResult["ITEMS"] as $item) {
		$sum += $item["PRICE"] * $item["QUANTITY"];
		$quantaty += $item["QUANTITY"];
	}
}
?>

<div class="header-cart">
	<div class="mini-cart">
		<span uk-icon="icon: cart; ratio: 2"></span>
		<span class="uk-badge"><?=$quantaty;?></span>
	</div>
	<div class="header-cart__right">
		<div class="header-cart__price">
			<?=$sum;?> &#8381;
		</div>
		<a href="<?=$arParams["PATH_TO_BASKET"]?>" class="header-cart__link">В корзину</a>
	</div>
</div>