<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
	<div class="leftnav">
		<?foreach($arResult as $mainNav){?>
			<ul>
				<?foreach($mainNav["CHILDREN"] as $leftSub){?>
					<li><a href="<?=$leftSub["LINK"]?>" <?=$leftSub["SELECTED"]==1?"class='active'":""?>><?=$leftSub["TEXT"]?></a></li>
					<a href=""></a>
				<?}?>
			</ul>
		<?}?>
	</div>
<?}?>
