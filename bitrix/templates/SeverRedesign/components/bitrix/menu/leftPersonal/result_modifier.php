<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$resultArray = array();
if(count($arResult)){
	$DL = 0;
	foreach($arResult as $resV){
		$currentDL = intval($resV["DEPTH_LEVEL"]);
		if($currentDL>=$DL){
			$DL = $currentDL;
			$resultArray[$DL][] = $resV;
		}else{
			while($DL>$currentDL){
				$prevDL = $DL-1;
				$lastElemOfPrevDL = count($resultArray[$prevDL])-1;
				$resultArray[$prevDL][$lastElemOfPrevDL]["CHILDREN"] = $resultArray[$DL];
				unset($resultArray[$DL]);
				$DL--;
			}
			$resultArray[$currentDL][] = $resV;
		}
	}
	while($DL>1){
		$prevDL = $DL-1;
		$lastElemOfPrevDL = count($resultArray[$prevDL])-1;
		$resultArray[$prevDL][$lastElemOfPrevDL]["CHILDREN"] = $resultArray[$DL];
		$resultArray[$DL] = array();
		$DL--;
	}
	$arResult = reset($resultArray);
}
?>