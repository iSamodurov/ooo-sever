<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
	<div class="footer_col footer-navigation">
		<b class="footer__title">Навигация</b>
		<ul>
			<?foreach($arResult as $arItem){?>
				<?if($arItem["SELECTED"]){?>
					<li><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
				<?}else{?>
					<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?}?>
			<?}?>
		</ul>
	</div>
<?}?>