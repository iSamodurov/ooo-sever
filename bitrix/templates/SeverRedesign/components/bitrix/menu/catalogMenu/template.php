<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)){?>
	<ul class="navigation-list">
		<?foreach($arResult as $mainNav){?>
			<li>
				<a href="<?=$mainNav["LINK"]?>" class=""><?=$mainNav["TEXT"]?></a>
				<div class="navigation-subnav">
					<ul class="navigation-categories">
						<?foreach($mainNav["CHILDREN"] as $leftSub){?>
							<li>
								<a href="<?=$leftSub["LINK"]?>"><?=$leftSub["TEXT"]?></a>
								<ul>
									<?foreach($leftSub["CHILDREN"] as $rightSub){?>
										<li><a href="<?=$rightSub["LINK"]?>"><?=$rightSub["TEXT"]?></a></li>
									<?}?>
								</ul>
							</li>
						<?}?>
					</ul>
				</div>
			</li>
		<?}?>
	</ul>
<?}?>