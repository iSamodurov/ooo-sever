<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn .= '<ul class="breadcrumbs">';
$cnt = count($arResult);

foreach($arResult as $k => $bread) {
	if($cnt-1 == $k){
		$strReturn .= '<li><span href="'.$bread["LINK"].'">'.$bread["TITLE"].'</span></li>';
	}else{
		$strReturn .= '<li><a href="'.$bread["LINK"].'">'.$bread["TITLE"].'</a></li>';
	}
}
$strReturn .= '</ul>';
return $strReturn;
?>
