<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru" xml:lang="ru">
	<head>
		<title><? $APPLICATION->ShowTitle() ?> </title>
		<? $APPLICATION->ShowHead(); ?>
		<meta name='yandex-verification' content='4d4ae873d94f8e80' />
		<meta name="google-site-verification" content="nQW0bsy7eUReUyZhKimu-HVwp3bBI8fijFyaufGmCsw" />
		<link rel="shortcut icon" type="image/x-icon" href="<?= SITE_TEMPLATE_PATH ?>/favicon.ico" />
		<link type="text/css" rel="stylesheet" href="/css/arcticmodal.css" />
		<link type="text/css" rel="stylesheet" href="/css/arcticmodal.simple.css" />
		<link type="text/css" rel="stylesheet" href="/css/style.css" />

		<!-- <script type="text/javascript" src="/js/dev_jquery.js"></script> -->

		<link type="text/css" rel="stylesheet" href="/css/jquery-ui.min.css" />
		<link type="text/css" rel="stylesheet" href="/js/fancybox/jquery.fancybox.css" />

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>		
		<script type="text/javascript" src="/js/fancybox/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="/js/jquery.bxslider.min.js"></script>
		
		<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/flux.min.js"></script>

		<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/js/script.js"></script>
		<script type="text/javascript">
		$(document).ready(function () {

			// Слайдер для фильтра по выбору цены
			$(".rangeslider").each(function () {
				var self = this;
				var min = $(this).attr('data-min');
				var max = $(this).attr('data-max');
				var curmin = $(this).attr('data-cur-min');
				var curmax = $(this).attr('data-cur-max');
				var $elmin = $("#" + $(this).attr('data-el-min'));
				var $elmax = $("#" + $(this).attr('data-el-max'));
				if ($(this).attr("id") == "innerlength") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-length"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				if ($(this).attr("id") == "outerlength") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-length"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				
				if ($(this).attr("id") == "innerwidth") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-width"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				if ($(this).attr("id") == "outerwidth") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-width"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				
				if ($(this).attr("id") == "innerheight") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-height"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				if ($(this).attr("id") == "outerheight") {
					var param = $(self).parent().parent();
					var minlength = curmax;
					var maxlength = curmin;
					$(".block_area_param > input:checked", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-height"));
							if (curlength < minlength) minlength = curlength;
							if (curlength > maxlength) maxlength = curlength;
						}
					);
					if (minlength != curmax) {
						curmax = maxlength;
					}
					if (maxlength != curmin) {
						curmin = minlength;
					}
				}
				
				$(self).slider({
					range: true,
					min: parseFloat(min),
					max: parseFloat(max),
					values: [parseFloat(curmin), parseFloat(curmax)],
					slide: function (event, ui) {
						$elmin.val(parseFloat(ui.values[0]));
						$elmax.val(parseFloat(ui.values[1]));
						//------------------------------
						var param = $(self).parent().parent();
						var length = $(".length .rangeslider", param).slider("values");
						var width = $(".width .rangeslider", param).slider("values");
						var height = $(".height .rangeslider", param).slider("values");
						//				console.log(param, length, width, height);
						$(".block_area_param > input", param).each(
							function() {
								var curlength = parseFloat($(this).attr("data-length"));
								var curwidth = parseFloat($(this).attr("data-width"));
								var curheight = parseFloat($(this).attr("data-height"));
								if (
									curlength >= length[0] && curlength <= length[1] &&
									curwidth >= width[0] && curwidth <= width[1] &&
									curheight >= height[0] && curheight <= height[1]
								) {
									$(this).prop("checked", true);
								} else {
									$(this).prop("checked", false);
								}
							}
						);
						//------------------------------
					}
				});
				$elmin.on("change", function (event) {
					var value;
					var curmin = parseFloat(event.target.value);
					var curmax = parseFloat($elmax.val());
					if (curmin < min) {
						value = min;
						event.target.value = value;
					} else if (curmin > curmax) {
						value = curmax;
						event.target.value = value;
					} else {
						value = curmin;
					}
					//------------------------------
					var param = $(self).parent().parent();
					var length = $(".length .rangeslider", param).slider("values");
					var width = $(".width .rangeslider", param).slider("values");
					var height = $(".height .rangeslider", param).slider("values");
					//			console.log(param, length, width, height);
					$(".block_area_param > input[type='checkbox']", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-length"));
							var curwidth = parseFloat($(this).attr("data-width"));
							var curheight = parseFloat($(this).attr("data-height"));
							if (
								curlength >= length[0] && curlength <= length[1] &&
								curwidth >= width[0] && curwidth <= width[1] &&
								curheight >= height[0] && curheight <= height[1]
							) {
								$(this).prop("checked", true);
							} else {
								$(this).prop("checked", false);
							}
						}
					);
					//------------------------------
					$(self).slider("values", 0, value);
				});
				$elmax.on("change", function (event) {
					var value;
					var curmin = parseFloat($elmin.val());
					var curmax = parseFloat(event.target.value);
					if (curmax > max) {
						value = max;
						event.target.value = value;
					} else if (curmax < curmin) {
						value = curmin;
						event.target.value = value;
					} else {
						value = curmax;
					}
					//------------------------------
					var param = $(self).parent().parent();
					var length = $(".length .rangeslider", param).slider("values");
					var width = $(".width .rangeslider", param).slider("values");
					var height = $(".height .rangeslider", param).slider("values");
					//			console.log(param, length, width, height);
					$(".block_area_param > input[type='checkbox']", param).each(
						function() {
							var curlength = parseFloat($(this).attr("data-length"));
							var curwidth = parseFloat($(this).attr("data-width"));
							var curheight = parseFloat($(this).attr("data-height"));
							if (
								curlength >= length[0] && curlength <= length[1] &&
								curwidth >= width[0] && curwidth <= width[1] &&
								curheight >= height[0] && curheight <= height[1]
							) {
								$(this).prop("checked", true);
							} else {
								$(this).prop("checked", false);
							}
						}
					);
					//------------------------------
					$(self).slider("values", 1, value);
				});
			});


			/*
			$('.slider ul').bxSlider({
				auto: true,
				speed: 2000,
				pause: 5000,
				controls: false,
				mode: 'fade'
			});
			*/
			$('div.cat div.pan ul').bxSlider({
				auto: true,
				speed: 500,
				pause: 4000,
				pager: false,
				controls: true,
				minSlides: 5,
				maxSlides: 5,
				slideWidth: 184,
				slideMargin: 12,
				moveSlides: 1,
				infiniteLoop: true
			});
			$('.vend div ul').bxSlider({
				minSlides: 6,
				maxSlides: 6,
				moveSlides: 1,
				slideWidth: 150,
				slideMargin: 10,
				pager: false,
				infiniteLoop: true
			});
			$('.card .photo ul').bxSlider({
				mode: 'fade',
				pagerCustom: '.view-pager',
				controls: false
			});
			$('.view-pager').bxSlider({
				minSlides: 4,
				maxSlides: 4,
				slideWidth: 80,
				slideMargin: 7,
				controls: false,
				pager: false,
				infiniteLoop: false
			});
			$('.fancybox').fancybox({
				closeClick: true
			});
			$('a#callback1').fancybox({
				'autoDimensions': false,
				'width': 400,
				'height': 500,
				'autoScale': true,
				'transitionIn': 'none',
				'transitionOut': 'none',
				'type': 'iframe',
				'titleShow': false
			});
			$('a#callback2').fancybox({
				'autoDimensions': false,
				'width': 400,
				'height': 500,
				'autoScale': true,
				'transitionIn': 'none',
				'transitionOut': 'none',
				'type': 'iframe',
				'titleShow': false
			});
			$('a#powcalc').fancybox({
				'autoDimensions': false,
				'width': 400,
				'height': 450,
				'autoScale': true,
				'transitionIn': 'none',
				'transitionOut': 'none',
				'type': 'iframe',
				'titleShow': false
			});
			// $('.cat .tab-ctrl a#tab1').click();
			var curr_tab = $('.cat .tab-ctrl .curr a').prop('hash');
			var height_b = $('.cat .tab-cont > div' + curr_tab).height();
			$('.cat div.tab-cont').height(height_b);
			$('.cat .tab-cont > div' + curr_tab).addClass('top-layer');
		});
		</script>

<!-- START ME-TALK -->
<script type='text/javascript'>
	(function(d, w, m) {
		var s = d.createElement('script');
		s.type ='text/javascript'; s.id = 'supportScript'; s.charset = 'utf-8';	s.async = true;
		s.src = '//me-talk.ru/support/support.js?h=fc64a0b7496c1a005fbf3ead5ca88a23';
		var sc = d.getElementsByTagName('script')[0];
		w[m] = w[m] || function() { (w[m].q = w[m].q || []).push(arguments); };
		if (sc) sc.parentNode.insertBefore(s, sc); else d.documentElement.firstChild.appendChild(s);
	})(document, window, 'MeTalk');
</script>
<!-- END ME-TALK -->

	</head>
	<body>
		<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
		<div class="wrapper">
			<div class="top">
				<div class="width">
					<div class="online-shop">ОСНАЩАЕМ И ОБСЛУЖИВАЕМ МАГАЗИНЫ • СТОЛОВЫЕ • КАФЕ • ГОСТИНИЦЫ И РЕСТОРАНЫ • ПИЩЕВЫЕ СКЛАДЫ И КОМБИНАТЫ</div>
				</div>
			</div>
			<div class="header">
				<div style="display:none">88006853123</br>+7495 873 0957</div>
				<div class="width">
					<div class="row-1 cl">
						<div class="logo left">
							<a href="/" title=""></a>
						</div>
						<div class="nav-contacts right">
							<div class="nav cl">
								<ul class="cl right">
									<li class="home"><a title="" href="/"></a></li>
									<li class="sitemap"><a title="" href="/search/map/"></a></li>
									<li class="email last"><a title="" href="/about/contacts/"></a></li>
								</ul>
							</div>
							<div class="contacts">
								<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/telephone.php"), false); ?>
							</div>
						</div>
						<div class="callback right">
							<a href="javascript:void(0);" onclick="$('#callbackModal').arcticmodal();" title="Обратный звонок">Обратный звонок</a>
						</div>
						<?$APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "basket_small", Array(
						"PATH_TO_BASKET" => "/personal/cart/",  // Страница корзины
						"PATH_TO_ORDER" => "/personal/order/",  // Страница оформления заказа
						"SHOW_DELAY" => "N",  // Показывать отложенные товары
						"SHOW_NOTAVAIL" => "N",  // Показывать товары, недоступные для покупки
						"SHOW_SUBSCRIBE" => "N",  // Показывать товары, на которые подписан покупатель
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "36000000",
						),
						false
						);?>
						<div class="auth right">
							<? if ($USER->IsAuthorized()): ?>
							<a href="/personal/">Ваш профиль</a> <span>/</span> <a href="?logout=yes">Выйти</a>
							<? else: ?>
							<a href="/auth/" title="">Вход</a><span>/</span><a href="/auth/?register=yes" title="">Регистрация</a>
							<?endif; ?>
						</div>
					</div>
					<div class="row-2">
						<div class="wrap cl">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MAX_LEVEL" => "3",
							"CHILD_MENU_TYPE" => "sub",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
							);?>
							<? if (!CSite::InDir("/catalog/") && !CSite::InDir("/components/")): ?>
							<?$APPLICATION->IncludeComponent("bitrix:search.title", "search_title", array(
							"NUM_CATEGORIES" => "2",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "Y",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => "/search/",
							"CATEGORY_0_TITLE" => "Продукция",
							"CATEGORY_0" => array(
							0 => "iblock_catalog",
							),
							"CATEGORY_0_iblock_catalog" => array(
							0 => "3",
							),
							"CATEGORY_1_TITLE" => "Комплектующие",
							"CATEGORY_1" => array(
							0 => "iblock_1c_catalog",
							),
							"CATEGORY_1_iblock_1c_catalog" => array(
							0 => "5",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search"
							),
							false
							);?>
							<? elseif (CSite::InDir("/catalog/")): ?>
							<?$APPLICATION->IncludeComponent("bitrix:search.title", "search_title", array(
							"NUM_CATEGORIES" => "2",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "Y",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => "/search/",
							"CATEGORY_0_TITLE" => "Продукция",
							"CATEGORY_0" => array(
							0 => "iblock_catalog",
							),
							"CATEGORY_0_iblock_catalog" => array(
							0 => "3",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search"
							),
							false
							);?>
							<?
							elseif (CSite::InDir("/components/")): ?>
							<?$APPLICATION->IncludeComponent("bitrix:search.title", "search_title", array(
							"NUM_CATEGORIES" => "2",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "Y",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => "/search/",
							"CATEGORY_0_TITLE" => "Комплектующие",
							"CATEGORY_0" => array(
							0 => "iblock_1c_catalog",
							),
							"CATEGORY_0_iblock_1c_catalog" => array(
							0 => "5",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search"
							),
							false
							);?>
							<?endif; ?>
						</div>
					</div>


					<div class="hidden_nav">
						<div class="width">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
								"ROOT_MENU_TYPE" => "top",
								"MENU_CACHE_TYPE" => "N",
								"MENU_CACHE_TIME" => "36000000",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"MENU_CACHE_GET_VARS" => array(
								),
								"MAX_LEVEL" => "1",
								"CHILD_MENU_TYPE" => "sub",
								"USE_EXT" => "Y",
								"DELAY" => "N",
								"ALLOW_MULTI_SELECT" => "N"
								),
								false
							);?>
						</div>
					</div>



				</div>
			</div>
			<div class="body">
				<div class="width">
					<? if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php' && $APPLICATION->GetCurPage() != '/catalog/' && $APPLICATION->GetCurPage() != '/catalog/index.php' && $APPLICATION->GetCurPage() != '/components/' && $APPLICATION->GetCurPage() != '/components/index.php'): ?>
					<div class="section cl">
						<div class="sidebar left">

							<!-- ---- SMART FILTER ---- -->
							<?$APPLICATION->ShowViewContent("left_area")?>


							<?$APPLICATION->IncludeComponent("bitrix:menu", "left_menu", array(
							"ROOT_MENU_TYPE" => "left",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(),
							"MAX_LEVEL" => "4",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N"
							),
							false
							);?>


							<?
							/*
							<div class="banner block">
								<a href="#" title=""><img src="img/banner.png" alt="" /></a>
							</div>
							*/
							?>


							<div class="faq block">
								<a href="/faq/" title="Вопросы и ответы">Вопросы и ответы</a>
							</div>
							<div class="credit block">
								<a href="/credit/" title="Покупка в кредит">Покупка в кредит</a>
							</div>
						</div>
						<div class="main right">
							<? endif; ?>
							<? if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php'): ?>
							<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb", array(
							"START_FROM" => "1",
							"PATH" => "",
							"SITE_ID" => "-"
							),
							false,
							Array('HIDE_ICONS' => 'Y')
							);?>
							<h1><? $APPLICATION->ShowTitle(true); ?></h1>
							<? endif; ?>