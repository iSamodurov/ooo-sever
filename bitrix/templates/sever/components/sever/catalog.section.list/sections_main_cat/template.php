<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<ul class="cl">
<?$i=0;?>
	<?
	foreach($arResult["SECTIONS"] as $key => $arSection)
	{
		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
		$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
		?>
		<li id="<?=$this->GetEditAreaId($arSection['ID']);?>"<?if($key==count($arResult["SECTIONS"])-1 || $i==4):?> class="last"<?endif;?>>
			<a href="<?=$arSection["SECTION_PAGE_URL"]?>" title="<?=$arSection["NAME"]?>">
				<?if($arSection["PICTURE"]["SRC"]!=''):?>
				<span class="img">
					<img src="<?=$arSection["PICTURE"]["SRC"]?>" alt="<?=$arSection["NAME"]?>" />
				</span>
				<?endif;?>
				<span class="link"><?=$arSection["NAME"]?></span>
			</a>
		</li>
		
		<?
		
	}
	?>
</ul>