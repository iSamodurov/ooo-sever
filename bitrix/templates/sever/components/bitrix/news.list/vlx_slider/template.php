<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? if (count($arResult["ITEMS"]) < 1)
return;
?>




<!--<div class="slider section" id="vlx_slider">-->
<div id="vlx_slider">
	<ul>
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]!=''):?>
				<li>
					<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];?>">
						<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem["NAME"];?>" title="<?=$arItem["NAME"];?>" />
					</a>
				</li>
				<?else:?>
				<li>
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem["NAME"];?>" title="<?=$arItem["NAME"];?>" />
				</li>
			<?endif;?>
		<?endforeach;?>
	</ul>
</div>

<script type="text/javascript">
	$(function(){
	    window.myFlux = new flux.slider('#vlx_slider', {
	    	pagination: true,
	    	autoplay: true,
	    	controls: false,
	    	delay: 6000,
	    	transitions: ['blinds3d', 'tiles3d']
	    });
	});
</script>