<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (count($arResult) > 0):?>
<h3>Просмотренные товары</h3>
						<div class="prod-list section">
							<div class="row cl last">
								<?$i=0;?>
	<?foreach($arResult as $arItem):?>

		
									<div class="item left<?=$i==3?' last':''?>">
										<div class="img">
											<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PICTURE"]["src"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"></a>
										</div>
										<div class="link">
											<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
										</div>
										<div class="char-list">
											<ul>
												<?if($elem["PROPERTY_ARTNUMBER_VALUE"]!=''):?>
												<li>
													<span class="left">Артикул:</span>
													<span class="right"><?=$elem["PROPERTY_ARTNUMBER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_MANUFACTURER_VALUE"]!=''):?>
												<li>
													<span class="left">Производитель:</span>
													<span class="right"><?=$elem["PROPERTY_MANUFACTURER_VALUE"];?></span>
												</li>
												<?endif;?>
												<?if($elem["PROPERTY_POWER_VALUE"]!=''):?>
												<li>
													<span class="left">Мощность:</span>
													<span class="right"><?=$elem["PROPERTY_POWER_VALUE"];?></span>
												</li>
												<?endif;?>
												
											</ul>
										</div>
										<div class="price"><?=$arItem["PRICE_FORMATED"]?></div>
										<div class="buy">
											<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" title="">Купить</a>
										</div>
									</div>
									
									<?$i++;?>		
	<?endforeach;?>
</div>
</div>
<?endif;?>