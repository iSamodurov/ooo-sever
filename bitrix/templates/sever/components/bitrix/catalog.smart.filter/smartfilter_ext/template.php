<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php if (count($arResult["ITEMS"]) > 0): ?>
	<?php CJSCore::Init(array("fx")); ?>

	<div class="bx_filter_horizontal" style="position: relative;">
	<div class="bx_filter_section m4">
	<div class="bx_filter_title"><?php echo GetMessage("CT_BCSF_FILTER_TITLE") ?></div>
	<form name="<?php echo $arResult["FILTER_NAME"] . "_form" ?>" action="<?php echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter">
	<div class="cl column-layout">
	<?php foreach ($arResult["HIDDEN"] as $arItem): ?>
		<input type="hidden" name="<?php echo $arItem["CONTROL_NAME"] ?>" id="<?php echo $arItem["CONTROL_ID"] ?>" value="<?php echo $arItem["HTML_VALUE"] ?>" />
	<?php endforeach; ?>

	<?php $i = 0 ?>
	<?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
		<?php if (isset($arItem["PRICE"])): ?>
			<?php if ((empty($arItem["VALUES"]["MIN"]["VALUE"]) && empty($arItem["VALUES"]["MAX"]["VALUE"])) || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"]): ?>
				<!--				--><?php //$i++ ?>
				<?php continue ?>
			<?php endif ?>
			<div class="bx_filter_container price clearfix column-break-avoid">
				<span class="bx_filter_container_title">Цена</span>

				<div class="bx_filter_param_area clearfix">
					<div class="bx_filter_param_area_block">От:
						<div class="bx_input_container">
							<input class="min-price" type="text" size="5"
							       name="<?php echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
							       id="<?php echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
							       value="<?php echo round(!empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"]) ?>" />
						</div>
					</div>
					<div class="bx_filter_param_area_block">до:
						<div class="bx_input_container">
							<input class="max-price" type="text" size="5"
							       name="<?php echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
							       id="<?php echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
							       value="<?php echo round(!empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]) ?>" />
						</div>
					</div>
				</div>
			</div>
			<?php $i++ ?>
		<?php endif ?>
	<?php endforeach ?>

	<?php foreach ($arResult["ITEMS"] as $key => $arItem): ?>
	<?php if ($i == 3): ?>
		<a href="#" class="showfilter clearfix" style="clear: both;position: absolute;top: 0;right: 0;">Расширенный подбор</a>

	<div class="hide showfilter clearfix">
	<?php endif; ?>
	<?php if ($arItem["PROPERTY_TYPE"] == "N"): ?>
		<?php $hidden = false ?>
		<?php if ((empty($arItem["VALUES"]["MIN"]["VALUE"]) && empty($arItem["VALUES"]["MAX"]["VALUE"])) || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"]): ?>
			<?php $i++ ?>
			<?php continue ?>
		<?php endif ?>
		<div class="bx_filter_container column-break-avoid price<?php echo $hidden ? ' hide' : '' ?>">
			<span class="bx_filter_container_title"><?= $arItem["NAME"] ?></span>

			<div class="bx_filter_param_area clearfix">
				<div class="bx_filter_param_area_block">От:
					<div class="bx_input_container">
						<input class="min-price" type="text" size="5"
						       name="<?php echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
						       id="<?php echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
						       value="<?php echo round(!empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"]) ?>" />
					</div>
				</div>
				<div class="bx_filter_param_area_block">до:
					<div class="bx_input_container">
						<input class="max-price" type="text" size="5"
						       name="<?php echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
						       id="<?php echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
						       value="<?php echo round(!empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]) ?>" />
					</div>
				</div>
			</div>

			<div class="rangeslider"
			     data-min="<?php echo round($arItem["VALUES"]["MIN"]["VALUE"]) ?>"
			     data-max="<?php echo round($arItem["VALUES"]["MAX"]["VALUE"]) ?>"
			     data-cur-min="<?php echo round(!empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"]) ?>"
			     data-cur-max="<?php echo round(!empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"]) ?>"
			     data-el-min="<?php echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
			     data-el-max="<?php echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>">
			</div>

		</div>

	<?php elseif (!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])): ?>
		<?php $hidden = false ?>
		<?php if ($arItem['CODE'] == 'PROP_INNERBLOCKSIZE' || $arItem['CODE'] == 'PROP_OUTERBLOCKSIZE'): ?>
			<?
			$minlength = 9999999;
			$maxlength = 0;
			$minwidth = 9999999;
			$maxwidth = 0;
			$minheight = 9999999;
			$maxheight = 0;

			$curminlength = 9999999;
			$curmaxlength = 0;
			$curminwidth = 9999999;
			$curmaxwidth = 0;
			$curminheight = 9999999;
			$curmaxheight = 0;
			?>

			<?php foreach ($arItem["VALUES"] as $val => $ar): ?>
				<?php $matches = array(); ?>
				<?php if (preg_match('/(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)/smi', $ar["VALUE"], $matches)): ?>
					<?php if ((float)$matches[1] < $minlength) $minlength = (float)$matches[1] ?>
					<?php if ((float)$matches[1] > $maxlength) $maxlength = (float)$matches[1] ?>
					<?php if ((float)$matches[2] < $minwidth) $minwidth = (float)$matches[2] ?>
					<?php if ((float)$matches[2] > $maxwidth) $maxwidth = (float)$matches[2] ?>
					<?php if ((float)$matches[3] < $minheight) $minheight = (float)$matches[3] ?>
					<?php if ((float)$matches[3] > $maxheight) $maxheight = (float)$matches[3] ?>

					<?php if ($ar["CHECKED"] && !$ar["DISABLED"]): ?>
						<?php if ((float)$matches[1] < $curminlength) $curminlength = (float)$matches[1] ?>
						<?php if ((float)$matches[1] > $curmaxlength) $curmaxlength = (float)$matches[1] ?>
						<?php if ((float)$matches[2] < $curminwidth) $curminwidth = (float)$matches[2] ?>
						<?php if ((float)$matches[2] > $curmaxwidth) $curmaxwidth = (float)$matches[2] ?>
						<?php if ((float)$matches[3] < $curminheight) $curminheight = (float)$matches[3] ?>
						<?php if ((float)$matches[3] > $curmaxheight) $curmaxheight = (float)$matches[3] ?>
					<?php endif ?>
				<?php endif ?>
			<?php endforeach ?>

			<?php if ($maxlength < $curminlength) $curminlength = $minlength ?>
			<?php if ($minlength > $curmaxlength) $curmaxlength = $maxlength ?>
			<?php if ($maxwidth < $curminwidth) $curminwidth = $minwidth ?>
			<?php if ($minwidth > $curmaxwidth) $curmaxwidth = $maxwidth ?>
			<?php if ($maxheight < $curminheight) $curminheight = $minheight ?>
			<?php if ($minheight > $curmaxheight) $curmaxheight = $maxheight ?>
			
			<?php if ($minlength == $maxlength && $minwidth == $maxwidth && $minheight == $maxheight): ?>
				<?php $hidden = true ?>
			<?php endif ?>
		<?php endif ?>

		<div class="bx_filter_container column-break-avoid<?php echo $hidden ? ' hide' : '' ?>">
		<span class="bx_filter_container_title"><?= $arItem["NAME"] ?></span>

		<?php if ($arItem['CODE'] == 'PROP_INNERBLOCKSIZE'): ?>
			<div>
				<div class="bx_filter_container length price<?php echo $minlength == $maxlength ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Длина</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="innerMinLength"
								       value="<?php echo $curminlength ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="innerMaxLength"
								       value="<?php echo $curmaxlength ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="innerlength"
					     data-min="<?php echo $minlength ?>"
					     data-max="<?php echo $maxlength ?>"
					     data-cur-min="<?php echo $curminlength ?>"
					     data-cur-max="<?php echo $curmaxlength ?>"
					     data-el-min="innerMinLength"
					     data-el-max="innerMaxLength">
					</div>
				</div>
				<div class="bx_filter_container width price<?php echo $minwidth == $maxwidth ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Ширина</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="innerMinWidth"
								       value="<?php echo $curminwidth ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="innerMaxWidth"
								       value="<?php echo $curmaxwidth ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="innerwidth"
					     data-min="<?php echo $minwidth ?>"
					     data-max="<?php echo $maxwidth ?>"
					     data-cur-min="<?php echo $curminwidth ?>"
					     data-cur-max="<?php echo $curmaxwidth ?>"
					     data-el-min="innerMinWidth"
					     data-el-max="innerMaxWidth">
					</div>
				</div>
				<div class="bx_filter_container height price<?php echo $minheight == $maxheight ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Высота</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="innerMinHeight"
								       value="<?php echo $curminheight ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="innerMaxHeight"
								       value="<?php echo $curmaxheight ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="innerheight"
					     data-min="<?php echo $minheight ?>"
					     data-max="<?php echo $maxheight ?>"
					     data-cur-min="<?php echo $curminheight ?>"
					     data-cur-max="<?php echo $curmaxheight ?>"
					     data-el-min="innerMinHeight"
					     data-el-max="innerMaxHeight">
					</div>
				</div>
				<div class="block_area_param" style="display: none">
					<?php foreach ($arItem["VALUES"] as $val => $ar): ?>
						<?php $matches = array() ?>
						<?php if (preg_match('/(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)/smi', $ar["VALUE"], $matches)): ?>
							<input type="checkbox"
							       value="<?php echo $ar["HTML_VALUE"] ?>"
							       name="<?php echo $ar["CONTROL_NAME"] ?>"
							       id="<?php echo $ar["CONTROL_ID"] ?>"
							       data-length="<?php echo $matches[1] ?>"
							       data-width="<?php echo $matches[2] ?>"
							       data-height="<?php echo $matches[3] ?>"
								<?php echo $ar["CHECKED"] ? 'checked' : '' ?>
								<?php echo $ar["DISABLED"] ? 'disabled' : '' ?> />
						<?php endif ?>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif ?>

		<?php if ($arItem['CODE'] == 'PROP_OUTERBLOCKSIZE'): ?>
			<div>
				<div class="bx_filter_container length price<?php echo $minlength == $maxlength ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Длина</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="outerMinLength"
								       value="<?php echo $curminlength ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="outerMaxLength"
								       value="<?php echo $curmaxlength ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="outerlength"
					     data-min="<?php echo $minlength ?>"
					     data-max="<?php echo $maxlength ?>"
					     data-cur-min="<?php echo $curminlength ?>"
					     data-cur-max="<?php echo $curmaxlength ?>"
					     data-el-min="outerMinLength"
					     data-el-max="outerMaxLength">
					</div>
				</div>
				<div class="bx_filter_container width price<?php echo $minwidth == $maxwidth ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Ширина</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="outerMinWidth"
								       value="<?php echo $curminwidth ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="outerMaxWidth"
								       value="<?php echo $curmaxwidth ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="outerwidth"
					     data-min="<?php echo $minwidth ?>"
					     data-max="<?php echo $maxwidth ?>"
					     data-cur-min="<?php echo $curminwidth ?>"
					     data-cur-max="<?php echo $curmaxwidth ?>"
					     data-el-min="outerMinWidth"
					     data-el-max="outerMaxWidth">
					</div>
				</div>
				<div class="bx_filter_container height price<?php echo $minheight == $maxheight ? ' hide' : '' ?>">
					<div class="bx_filter_container_title text-center">Высота</div>
					<div class="bx_filter_param_area clearfix" style="margin-bottom: 10px">
						<div class="bx_filter_param_area_block">От:
							<div class="bx_input_container">
								<input class="min-price" type="text" size="5"
								       id="outerMinHeight"
								       value="<?php echo $curminheight ?>" />
							</div>
						</div>
						<div class="bx_filter_param_area_block">до:
							<div class="bx_input_container">
								<input class="max-price" type="text" size="5"
								       id="outerMaxHeight"
								       value="<?php echo $curmaxheight ?>" />
							</div>
						</div>
					</div>
					<div class="rangeslider" id="outerheight"
					     data-min="<?php echo $minheight ?>"
					     data-max="<?php echo $maxheight ?>"
					     data-cur-min="<?php echo $curminheight ?>"
					     data-cur-max="<?php echo $curmaxheight ?>"
					     data-el-min="outerMinHeight"
					     data-el-max="outerMaxHeight">
					</div>
				</div>
				<div class="block_area_param" style="display: none">
					<?php foreach ($arItem["VALUES"] as $val => $ar): ?>
						<?php $matches = array() ?>
						<?php if (preg_match('/(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)\D+(\d+(?:\.\d+)?)/smi', $ar["VALUE"], $matches)): ?>
							<input type="checkbox"
							       value="<?php echo $ar["HTML_VALUE"] ?>"
							       name="<?php echo $ar["CONTROL_NAME"] ?>"
							       id="<?php echo $ar["CONTROL_ID"] ?>"
							       data-length="<?php echo $matches[1] ?>"
							       data-width="<?php echo $matches[2] ?>"
							       data-height="<?php echo $matches[3] ?>"
								<?php echo $ar["CHECKED"] ? 'checked' : '' ?>
								<?php echo $ar["DISABLED"] ? 'disabled' : '' ?> />
						<?php endif ?>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif ?>

		<?php if ($arItem['CODE'] != 'PROP_INNERBLOCKSIZE' && $arItem['CODE'] != 'PROP_OUTERBLOCKSIZE'): ?>
			<div class="bx_filter_block">
				<?php foreach ($arItem["VALUES"] as $val => $ar): ?>
					<div<?php echo $ar["DISABLED"] ? ' class="disabled"' : '' ?>>
						<input type="checkbox"
						       value="<?php echo $ar["HTML_VALUE"] ?>"
						       name="<?php echo $ar["CONTROL_NAME"] ?>"
						       id="<?php echo $ar["CONTROL_ID"] ?>"
							<?php echo $ar["CHECKED"] ? 'checked' : '' ?>
							<?php echo $ar["DISABLED"] ? 'disabled' : '' ?> /> <label for="<?php echo $ar["CONTROL_ID"] ?>"><?php echo $ar["VALUE"]; ?></label>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		</div>

	<?endif; ?>
	<?php $i++ ?>
	<?php endforeach; ?>
	<?php if ($i > 2): ?>
	</div>
	<?php endif ?>
	</div>
	<div class="bx_filter_control_section left">
		<input type="submit" id="set_filter" name="set_filter" value="<?= GetMessage("CT_BCSF_SET_FILTER") ?>" />
	</div>
	<div class="bx_filter_control_section">
		<input type="submit" id="del_filter" name="del_filter" value="<?= GetMessage("CT_BCSF_DEL_FILTER") ?>" />
	</div>
	</form>
	</div>
	</div>
	<script>
		var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
		if (!$("div.showfilter").children().length) {
			$("a.showfilter").hide();
		}
	</script>
<?php endif; ?>
