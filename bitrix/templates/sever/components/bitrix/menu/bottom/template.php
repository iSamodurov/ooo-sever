<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<?
$prevdepth = 0;
$classes = array("a", "b", "c", "d");
$i=0;

foreach($arResult as $arItem):
	if($arItem["DEPTH_LEVEL"] > 2) 
		continue;
	if($i>3 && $arItem["DEPTH_LEVEL"]==1) break;
?>
	<?if($arItem["DEPTH_LEVEL"]==1):?>
		<?if($arItem["DEPTH_LEVEL"]<$prevdepth):?>
		</ul>
		</div>
		<?endif;?>
		<div class="col <?=$classes[$i];?> left">
		<h4><?=$arItem["TEXT"]?></h4>
		<ul>
		<?$i++;?>
	<?else:?>
		<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif;?>

	<?$prevdepth = $arItem["DEPTH_LEVEL"];?>
<?endforeach?>
</ul>
</div>

<?endif?>