<div style="clear:both;"></div>
<?php global $vwres; ?>

				<? if ($APPLICATION->GetCurPage() != '/' && $APPLICATION->GetCurPage() != '/index.php' && $APPLICATION->GetCurPage() != '/catalog/' && $APPLICATION->GetCurPage() != '/catalog/index.php' && $APPLICATION->GetCurPage() != '/components/' && $APPLICATION->GetCurPage() != '/components/index.php'): ?>
			</div>
		</div>
	<? endif; ?>
		<div class="vend section">
			<?$APPLICATION->IncludeComponent("bitrix:news.list", "partners_bottom", array(
					"IBLOCK_TYPE" => "about",
					"IBLOCK_ID" => "6",
					"NEWS_COUNT" => "8",
					"SORT_BY1" => "SORT",
					"SORT_ORDER1" => "ASC",
					"SORT_BY2" => "SORT",
					"SORT_ORDER2" => "ASC",
					"FILTER_NAME" => "",
					"FIELD_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"CHECK_DATES" => "Y",
					"DETAIL_URL" => "/about/partners/#ID#/",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"AJAX_OPTION_HISTORY" => "N",
					"CACHE_TYPE" => "A",
					"CACHE_TIME" => "36000000",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"PREVIEW_TRUNCATE_LEN" => "",
					"ACTIVE_DATE_FORMAT" => "d.m.Y",
					"SET_TITLE" => "N",
					"SET_STATUS_404" => "N",
					"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
					"ADD_SECTIONS_CHAIN" => "N",
					"HIDE_LINK_WHEN_NO_DETAIL" => "N",
					"PARENT_SECTION" => "",
					"PARENT_SECTION_CODE" => "",
					"INCLUDE_SUBSECTIONS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"DISPLAY_TOP_PAGER" => "N",
					"DISPLAY_BOTTOM_PAGER" => "N",
					"PAGER_TITLE" => "РїС—Р…РїС—Р…РїС—Р…РїС—Р…РїС—Р…РїС—Р…РїС—Р…",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"DISPLAY_DATE" => "N",
					"DISPLAY_NAME" => "Y",
					"DISPLAY_PICTURE" => "Y",
					"DISPLAY_PREVIEW_TEXT" => "N",
					"AJAX_OPTION_ADDITIONAL" => ""
				),
				false
			);?>
		</div>
	</div>
</div>
<div class="footer-indent"></div>
</div>

<div class="footer">
	<div class="nav">
		<div class="wrap">
			<div class="width cl">
				<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "N",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "2",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
				<div class="col right">
					<div class="nav-2">
						<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/links_bottom.php"), false); ?>
					</div>
					<div class="subscr">
						<div class="label">Подпишитесь на рассылку новостей</div>
						<?$APPLICATION->IncludeComponent("bitrix:subscribe.form", "subscription", Array(
								"USE_PERSONALIZATION" => "Y", 
								"SHOW_HIDDEN" => "N",  
								"PAGE" => "#SITE_DIR#personal/subscribe/", 
								"CACHE_TYPE" => "A",  
								"CACHE_TIME" => "3600",  
							),
							false
						);?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="disc">
		<div class="width">
			<div class="cont">
				<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/discount.php"), false); ?>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="width cl">
			<div class="site left">
				<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/copyright.php"), false); ?>
			</div>
			<div class="contacts left">
				<div class="phone"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/phone_bottom.php"), false); ?></div>
				<div class="cl">
					<div class="callback left">
						<a onclick="$('#callbackModal').arcticmodal();" title="Заказать обратный звонок" style="cursor: pointer;">
							Заказать обратный звонок
						</a>
					</div>
					<div class="email right">
						<? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR . "include/mail_bottom.php"), false); ?>
					</div>
				</div>
			</div>
			<div class="dev right">
				<noindex><a href="http://vilnex.ru" title="VILNEX Studio">Сопровождение </a> &#151; VILNEX Studio</noindex>
			</div>
			<div class="soc right">
				<ul class="cl">
					<li class="a"><a href="#" title=""></a></li>
					<li class="b"><a href="#" title=""></a></li>
					<li class="c"><a href="#" title=""></a></li>
					<li class="d"><a href="#" title=""></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>



<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter31311128 = new Ya.Metrika({
                    id:31311128,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/31311128" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- google analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-49062438-2', 'auto');
  ga('send', 'pageview');

</script>
<!-- /gogle analytics -->


</div>
<?php $oldTitle = $APPLICATION->GetTitle(); ?>

<?php $newTitle = trim($newTitle); ?>
<?php $APPLICATION->SetTitle($newTitle); ?>


<!-- --------- STYLE ---------------------- -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<!-- -------------- CALLBACK FORM MODAL ---------- -->
<style type="text/css">
	.box-modal {width: 310px;}
	.box-modal input {
		display: block;
		padding: 5px 15px;
		margin-bottom: 25px;
	}
	.box-modal_close {
		width: 25px;
	    height: 25px;
	    background: rgb(66, 124, 212);
	    text-align: center;
	    line-height: 25px;
	    color: #fff;
	    border-radius: 50%;
	    top: -12px;
	    right: -12px;
	    font-size: 16px;
	    box-shadow: 1px 1px 5px rgba(0,0,0,0.5);
	}
	.box-modal_close:hover {color: #fff;}
	.box-modal_close:hover i.fa {
		transition: all 300ms;
		transform: rotate(90deg);
	}
	.box-modal_close i.fa {transition: all 300ms;}
	.box-modal label {margin-left: 13px; font-size: 12px; position: relative;}
	.box-modal input {
		display: table;
		margin: -1px auto 16px auto;
	    width: 78%;
	    padding: 10px 0px 10px 40px;
	    border-radius: 5px;
	    border: 1px solid #aaa;
	    box-shadow: inset 2px 3px 26px -8px rgba(0,0,0,0.2);
	    background: #fefefe;
	}

	.box-modal .btn {
		width: 92%;
	    padding: 10px;
	    margin: 0 auto;
	    display: table;
	    border: 1px solid #00388E;
	    background: #427CD4;
	    font-size: 14px;
	    border-radius: 5px;
	    color: #fff;
	    cursor: pointer;
	    text-transform: uppercase;
	}

	#nameLabel:after {
		font-family: FontAwesome;
	    content: "\f007";
	    position: absolute;
	    left: 12px;
	    top: 25px;
	    font-size: 18px;
	    opacity: 0.4;
	}
	#emailLabel:after {
		font-family: FontAwesome;
	    content: "\f0e0";
	    position: absolute;
	    left: 12px;
	    top: 25px;
	    font-size: 16px;
	    opacity: 0.4;
	}
	#phoneLabel:after {
		font-family: FontAwesome;
	    content: "\f095";
	    position: absolute;
	    left: 12px;
	    top: 26px;
	    font-size: 21px;
	    opacity: 0.4;
	}
</style>


<div style="display: none;">
	<div class="box-modal callback_modal" id="callbackModal">
		<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i></div>

		<form action="/vlx/submit.php" method="post" id="callback_form">
			<label id="nameLabel">Введите Имя:</label>
			<input type="text" name="name" id="boxName" placeholder="Иван Иванов:"></input>	
			<label id="emailLabel">Введите E-mail:</label>
			<input type="email" name="email" placeholder="ivanov@domen.ru:"></input>	
			<label id="phoneLabel">Номер телефона:</label>
			<input type="tel" name="phone" placeholder="+7 (123) 456-78-90"></input>	
			<button type="submit" class="btn">Заказать звонок</button>

			<input type="hidden" name="comment" value="Заказ обратного звонка из всплывающей формы"></input>

		</form>
	</div>
</div>





<!-- --------- SCRIPTS ---------------------- -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.9/jquery.mask.min.js"></script>
<script type="text/javascript" src="/js/jquery.arcticmodal.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[type=tel]").mask('+7 (000) 000-00-00');
	});


	$("#callback_form").submit(function(){
		var data = $(this).serialize();
		$.ajax({
			url: $(this).attr('action'),
			data: data,
			dataType: 'json',
			method: 'POST',
			beforeSend: function(){
				$.arcticmodal( 'close' );
				alert('Спасибо за ваше доверие! Наш менеджер скоро свяжется с вами.');
			}
		});
		return false;
	});

</script>

</body>
</html>