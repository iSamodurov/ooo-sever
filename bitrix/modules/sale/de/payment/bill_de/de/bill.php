<?
global $MESS;

$MESS["SBLP_DTITLE"] = "Invoice (Deutsch)";
$MESS["SBLP_DDESCR"] = "Rechnung zum Ausdrucken. Sie wird in neuem Fenster geцffnet.";
$MESS["SBLP_DATE"] = "Bestelldatum";
$MESS["SBLP_DATE_DESC"] = "Bestelldatum";
$MESS["SBLP_PAY_BEFORE"] = "Bezahlen bis";
$MESS["SBLP_PAY_BEFORE_DESC"] = "Bezahlen bis";
$MESS["SBLP_SUPPLI"] = "Lieferant";
$MESS["SBLP_SUPPLI_DESC"] = "Lieferant (Verkдufer)";
$MESS["SBLP_ADRESS_SUPPLI"] = "Adresse des Lieferanten";
$MESS["SBLP_ADRESS_SUPPLI_DESC"] = "Adresse des Lieferanten (Verkдufers)";
$MESS["SBLP_PHONE_SUPPLI"] = "Telefon des Lieferanten";
$MESS["SBLP_PHONE_SUPPLI_DESC"] = "Telefon des Lieferanten (Verkдufers)";
$MESS["SBLP_EMAIL_SUPPLI"] = "E-Mail des Lieferanten";
$MESS["SBLP_EMAIL_SUPPLI_DESC"] = "E-Mail des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_ACCNO_SUPPLI"] = "Kontonummer des Lieferanten";
$MESS["SBLP_BANK_ACCNO_SUPPLI_DESC"] = "Kontonummer des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_ACCNO_SUPPLI_VAL"] = "Kontonummer des Lieferanten";
$MESS["SBLP_BANK_SUPPLI"] = "Bank des Lieferanten";
$MESS["SBLP_BANK_SUPPLI_DESC"] = "Bank des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_SUPPLI_VAL"] = "Bank des Lieferanten";
$MESS["SBLP_BANK_BLZ_SUPPLI"] = "Bankverbindung des Lieferanten";
$MESS["SBLP_BANK_BLZ_SUPPLI_DESC"] = " Bankverbindung des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_BLZ_SUPPLI_VAL"] = " Bankverbindung des Lieferanten";
$MESS["SBLP_BANK_IBAN_SUPPLI"] = "IBAN des Lieferanten";
$MESS["SBLP_BANK_IBAN_SUPPLI_DESC"] = " IBAN des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_IBAN_SUPPLI_VAL"] = " IBAN des Lieferanten";
$MESS["SBLP_BANK_SWIFT_SUPPLI"] = "SWIFT des Lieferanten";
$MESS["SBLP_BANK_SWIFT_SUPPLI_DESC"] = "SWIFT des Lieferanten (Verkдufers)";
$MESS["SBLP_BANK_SWIFT_SUPPLI_VAL"] = "SWIFT des Lieferanten";
$MESS["SBLP_EU_INN_SUPPLI"] = "Umsatzsteuer-Identifikationsnummer";
$MESS["SBLP_EU_INN_SUPPLI_DESC"] = "Umsatzsteuer-Identifikationsnummer (Verkдufers)";
$MESS["SBLP_INN_SUPPLI"] = "Steuernummer";
$MESS["SBLP_INN_SUPPLI_DESC"] = "Steuernummer (Verkдufers)";
$MESS["SBLP_REG_SUPPLI"] = "Handelsregister";
$MESS["SBLP_REG_SUPPLI_DESC"] = "Handelsregister (beim Verkдufer) mit der Angabe der Eintragsnummer";
$MESS["SBLP_DIR_SUPPLI"] = "Geschдftsfьhrer des Lieferanten";
$MESS["SBLP_DIR_SUPPLI_DESC"] = "Geschдftsfьhrer des Lieferanten (Verkдufers)";
$MESS["SBLP_CUSTOMER_ID"] = "Kunden-ID";
$MESS["SBLP_CUSTOMER"] = "Kunde";
$MESS["SBLP_CUSTOMER_DESC"] = "Kunde (Kдufer)";
$MESS["SBLP_CUSTOMER_ADRES"] = "Adresse des Kunden";
$MESS["SBLP_CUSTOMER_ADRES_DESC"] = "Adresse des Kunden (Kдufers)";
$MESS["SBLP_CUSTOMER_PHONE"] = "Telefon des Kunden";
$MESS["SBLP_CUSTOMER_PHONE_DESC"] = "Telefon des Kunden (Kдufers)";
$MESS["SBLP_CUSTOMER_FAX"] = "Fax des Kunden";
$MESS["SBLP_CUSTOMER_FAX_DESC"] = "Fax des Kunden (Kдufers)";
$MESS["SBLP_CUSTOMER_PERSON"] = "Ansprechpartner beim Kunden";
$MESS["SBLP_CUSTOMER_PERSON_DESC"] = "Ansprechpartner beim Kunden (Kдufer)";

$MESS["SBLP_DIR_POS_SUPPLI"] = "Position des Leiters";
$MESS["SBLP_DIR_POS_SUPPLI_DESC"] = "Position des Leiters (Vertrieb)";
$MESS["SBLP_DIR_POS_SUPPLI_VAL"] = "Geschдftsfьhrer";
$MESS["SBLP_ACC_POS_SUPPLI"] = "Position des Buchhalters";
$MESS["SBLP_ACC_POS_SUPPLI_DESC"] = "Position des Buchhalters (Vertrieb)";
$MESS["SBLP_ACC_POS_SUPPLI_VAL"] = "Buchhalter";

$MESS["SBLP_DIR_SUPPLI"] = "Vor- und Nachname des Leiters";
$MESS["SBLP_DIR_SUPPLI_DESC"] = "Vor- und Nachname des Leiters (Vertrieb)";
$MESS["SBLP_ACC_SUPPLI"] = "Vor- und Nachname des Buchhalters";
$MESS["SBLP_ACC_SUPPLI_DESC"] = "Vor- und Nachname des Buchhalters (Vertrieb)";

$MESS["SBLP_PRINT"] = "Stempel";
$MESS["SBLP_PRINT_DESC"] = "Abbildung des Lieferantenstempels (Empfohlene GrцЯer: 150x150)";
$MESS["SBLP_LOGO"] = "Logo des Lieferanten";
$MESS["SBLP_LOGO_DESC"] = "Logo des Lieferanten  (Empfohlene GrцЯe: 80x80)";

$MESS["SBLP_DIR_SIGN_SUPPLI"] = "Unterschrift des Geschдftsfьhrers";
$MESS["SBLP_DIR_SIGN_SUPPLI_DESC"] = "Abbildung der Unterschrift des Geschдftsfьhrers (Empfohlene GrцЯe: 200x50)";
$MESS["SBLP_ACC_SIGN_SUPPLI"] = "Unterschrift des Buchhalters";
$MESS["SBLP_ACC_SIGN_SUPPLI_DESC"] = "Abbildung der Unterschrift des Buchhalters (Empfohlene GrцЯe: 200x50)";

$MESS["SBLP_BACKGROUND"] = "Hintergrund";
$MESS["SBLP_BACKGROUND_DESC"] = "Hintergrundbild fьr Rechnungen (Empfohlene GrцЯe: 800x1120)";

$MESS["SBLP_BACKGROUND_STYLE"] = "Hintergrunddarstellungstyp";
$MESS["SBLP_BACKGROUND_STYLE_NONE"] = "Kein";
$MESS["SBLP_BACKGROUND_STYLE_TILE"] = "Ausfьllen";
$MESS["SBLP_BACKGROUND_STYLE_STRETCH"] = "Volle GrцЯe";

$MESS["SBLP_MARGIN_TOP"] = "Oberes Feld";
$MESS["SBLP_MARGIN_RIGHT"] = "Rechtes Feld";
$MESS["SBLP_MARGIN_BOTTOM"] = "Unteres Feld";
$MESS["SBLP_MARGIN_LEFT"] = "Linkes Feld";

$MESS["SBLP_COMMENT1"] = "Kommentar zur Rechnung 1";
$MESS["SBLP_COMMENT2"] = "Kommentar zur Rechnung 2";
?>