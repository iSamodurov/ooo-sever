<?
$MESS["PRICE_VAT_SHOW_VALUE_TIP"] = "Diese Option zeigt die MwSt. an.";
$MESS["PATH_TO_ORDER_TIP"] = "Pfad zur Seite mit dem Bestellprozess. Wenn die Seite sich im gleichen Ordner wie auch die aktuelle befindet, reicht es, die Bezeichnung der Seite anzugeben.";
$MESS["COLUMNS_LIST_TIP"] = "Wдhlen Sie Felder, die als Spaltenьberschriften in der Produkttabelle angezeigt werden.";
$MESS["PRICE_VAT_INCLUDE_TIP"] = "Wenn diese Option aktiv ist, werden die Preise inklusive der MwSt. angezeigt.";
$MESS["SET_TITLE_TIP"] = "Wenn diese Option aktiviert ist, erscheint \"Mein Warenkorb\" als Ьberschrift";
$MESS["HIDE_COUPON_TIP"] = "Wenn Sie \"Ja\" auswдhlen, wird das Feld fьr die Eingabe des Gutscheincodes auf der Warenkorbseite nicht angezeigt.";
?>